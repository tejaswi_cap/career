-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2021 at 11:46 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cap`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `role` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT current_timestamp(),
  `updateddate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `location` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `username`, `mobile`, `email`, `password`, `image`, `role`, `status`, `createddate`, `updateddate`, `location`) VALUES
(4, 'Teja', 'teja', '9999999998', 'tejaswi@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 1, 1, '2020-12-01 18:30:07', '2020-12-15 16:20:28', 'Gadwal'),
(5, 'james', 'james', '9999999999', 'james@user.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 1, 1, '2020-12-21 21:58:27', '2020-12-21 16:28:27', NULL),
(6, 'jack', 'jack', '9999999999', 'jack@user.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 1, 1, '2020-12-21 21:58:35', '2020-12-21 16:29:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE `admin_login` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ipaddress` varchar(45) NOT NULL,
  `createddate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`id`, `user_id`, `ipaddress`, `createddate`) VALUES
(10, 4, '::1', '2020-12-01 18:30:24'),
(11, 4, '::1', '2020-12-01 19:05:35'),
(12, 4, '::1', '2020-12-01 19:42:44'),
(13, 4, '::1', '2020-12-02 11:42:00'),
(14, 4, '::1', '2020-12-02 13:16:38'),
(15, 4, '::1', '2020-12-02 13:25:10'),
(16, 4, '::1', '2020-12-02 16:02:48'),
(17, 4, '::1', '2020-12-14 18:57:18'),
(18, 4, '::1', '2020-12-14 21:31:03'),
(19, 4, '::1', '2020-12-15 12:36:14'),
(20, 4, '::1', '2020-12-15 12:36:58'),
(21, 4, '::1', '2020-12-15 12:38:12'),
(22, 4, '::1', '2020-12-15 13:10:54'),
(23, 4, '::1', '2020-12-15 13:29:26'),
(24, 4, '::1', '2020-12-15 13:40:27'),
(25, 4, '::1', '2020-12-15 13:48:16'),
(26, 4, '::1', '2020-12-15 14:06:06'),
(27, 4, '::1', '2020-12-15 21:40:21'),
(28, 4, '::1', '2020-12-15 21:40:53'),
(29, 4, '::1', '2020-12-15 21:50:04'),
(30, 4, '::1', '2020-12-15 21:53:36'),
(31, 4, '::1', '2020-12-15 21:55:59'),
(32, 4, '::1', '2020-12-16 19:58:22'),
(33, 4, '::1', '2020-12-16 20:01:10'),
(34, 4, '::1', '2020-12-16 20:21:46'),
(35, 4, '::1', '2020-12-17 19:22:39'),
(36, 4, '::1', '2020-12-18 08:46:51'),
(37, 4, '::1', '2020-12-21 21:34:06'),
(38, 4, '::1', '2020-12-22 13:06:14'),
(39, 4, '::1', '2020-12-22 18:08:23'),
(40, 4, '::1', '2020-12-23 10:26:29'),
(41, 4, '::1', '2020-12-24 11:38:58'),
(42, 4, '::1', '2020-12-28 19:44:40'),
(43, 4, '::1', '2021-01-04 12:52:31'),
(44, 4, '::1', '2021-01-04 19:57:29'),
(45, 4, '::1', '2021-01-05 20:00:20'),
(46, 4, '::1', '2021-01-06 10:01:34'),
(47, 4, '::1', '2021-01-06 21:04:39'),
(48, 4, '::1', '2021-01-07 19:13:01'),
(49, 4, '::1', '2021-01-10 17:22:33'),
(50, 4, '::1', '2021-01-11 13:05:20');

-- --------------------------------------------------------

--
-- Table structure for table `admin_otp`
--

CREATE TABLE `admin_otp` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `otp` varchar(45) NOT NULL,
  `createddate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_otp`
--

INSERT INTO `admin_otp` (`id`, `email`, `otp`, `createddate`) VALUES
(19, 'tejaswi@gmail.com', '290390', '2020-12-01 18:40:16'),
(20, 'tejaswi@gmail.com', '445805', '2020-12-01 18:40:22'),
(21, 'tejaswi@gmail.com', '656882', '2020-12-01 18:40:26'),
(22, 'tejaswi@gmail.com', '634196', '2020-12-15 16:04:20'),
(23, 'tejaswi@gmail.com', '426932', '2020-12-15 16:15:45'),
(24, 'tejaswi@gmail.com', '136034', '2020-12-15 16:16:53'),
(25, 'tejaswi@gmail.com', '370178', '2020-12-15 16:16:57'),
(26, 'tejaswi@gmail.com', '841085', '2020-12-15 16:17:01'),
(27, 'tejaswi@gmail.com', '899615', '2020-12-15 16:17:04'),
(28, 'tejaswi@gmail.com', '707608', '2020-12-15 16:18:15');

-- --------------------------------------------------------

--
-- Table structure for table `applied_candidates`
--

CREATE TABLE `applied_candidates` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'Awaiting review',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `applied_candidates`
--

INSERT INTO `applied_candidates` (`id`, `user_id`, `job_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 5, 9, 'Awaiting review', '2020-12-21 16:30:23', '2020-12-21 16:30:23'),
(2, 6, 5, 'Awaiting review', '2020-12-21 16:30:23', '2020-12-21 16:30:23'),
(3, 5, 2, 'Awaiting review', '2020-12-21 16:30:34', '2020-12-21 16:30:34'),
(4, 6, 9, 'Awaiting review', '2020-12-21 16:30:34', '2020-12-21 16:30:34');

-- --------------------------------------------------------

--
-- Table structure for table `college`
--

CREATE TABLE `college` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `location_id` int(11) NOT NULL,
  `sub_location` varchar(150) DEFAULT NULL,
  `fee` text DEFAULT NULL,
  `admissions` enum('open','close') NOT NULL DEFAULT 'close',
  `website` varchar(255) DEFAULT NULL,
  `logo` text DEFAULT NULL,
  `courses` text DEFAULT NULL,
  `year_of_establishment` varchar(50) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `affiliated_to` int(11) DEFAULT NULL,
  `admission_criteria` varchar(255) DEFAULT NULL,
  `library` enum('yes','no') DEFAULT NULL,
  `hostel` text DEFAULT NULL,
  `sports` enum('yes','no') DEFAULT NULL,
  `gym` enum('yes','no') DEFAULT NULL,
  `ac_campus` enum('yes','no') DEFAULT NULL,
  `transportation` text DEFAULT NULL,
  `computer_lab` enum('yes','no') DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `college`
--

INSERT INTO `college` (`id`, `name`, `location_id`, `sub_location`, `fee`, `admissions`, `website`, `logo`, `courses`, `year_of_establishment`, `type`, `approved_by`, `affiliated_to`, `admission_criteria`, `library`, `hostel`, `sports`, `gym`, `ac_campus`, `transportation`, `computer_lab`, `is_active`, `is_deleted`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(4, 'Indian Institute of Technology, Hyderabad', 4, 'Sangareddy', NULL, 'open', 'https://iith.ac.in/', '', NULL, '2008', 1, 0, 1, 'Entrance Test', 'no', NULL, 'yes', 'yes', 'no', NULL, 'no', 1, 0, 0, '2021-01-04 10:39:17', NULL, '2021-01-04 10:39:17');

-- --------------------------------------------------------

--
-- Table structure for table `college_brochure`
--

CREATE TABLE `college_brochure` (
  `id` int(11) NOT NULL,
  `college_id` int(11) NOT NULL,
  `file` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `college_brochure`
--

INSERT INTO `college_brochure` (`id`, `college_id`, `file`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, 4, 'http://localhost/cap/assets/company/brochure/MrMechanic_Founder_Proposal_Step-1_To_Step-16_20200923_011.pdf', 0, '2021-01-04 17:52:25', NULL, '2021-01-04 17:52:25'),
(2, 4, 'http://localhost/cap/assets/company/brochure/CTSS_Mr_MechanicMobileAppEstimates.pdf', 0, '2021-01-04 17:52:37', NULL, '2021-01-04 17:52:37');

-- --------------------------------------------------------

--
-- Table structure for table `college_course`
--

CREATE TABLE `college_course` (
  `id` int(11) NOT NULL,
  `college_id` int(11) NOT NULL,
  `qualification_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `yearly_fee` decimal(13,2) NOT NULL,
  `duration_years` int(11) NOT NULL,
  `duration_months` int(11) NOT NULL,
  `total_fee` decimal(13,2) NOT NULL,
  `academic_eligibility` int(11) NOT NULL,
  `entrance_test` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `college_course`
--

INSERT INTO `college_course` (`id`, `college_id`, `qualification_id`, `course_id`, `yearly_fee`, `duration_years`, `duration_months`, `total_fee`, `academic_eligibility`, `entrance_test`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(16, 4, 6, 4, '2014.00', 0, 0, '50.00', 1, 1, 4, '2021-01-07 16:55:09', NULL, '2021-01-07 16:55:09'),
(17, 4, 5, 7, '1992.00', 0, 0, '56.00', 1, 1, 4, '2021-01-07 16:55:18', NULL, '2021-01-07 16:55:18');

-- --------------------------------------------------------

--
-- Table structure for table `college_faculty`
--

CREATE TABLE `college_faculty` (
  `id` int(11) NOT NULL,
  `college_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `qualification_id` int(11) NOT NULL,
  `experience` varchar(50) DEFAULT NULL,
  `course_id` int(11) NOT NULL,
  `designation` int(11) NOT NULL,
  `image` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `college_placement`
--

CREATE TABLE `college_placement` (
  `id` int(11) NOT NULL,
  `college_id` int(11) NOT NULL,
  `tieup_companies` int(20) NOT NULL,
  `students_selected` int(20) NOT NULL,
  `batch_year` varchar(100) NOT NULL,
  `average_package` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `role` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT current_timestamp(),
  `updateddate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `location` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `name`, `username`, `mobile`, `email`, `password`, `image`, `role`, `status`, `createddate`, `updateddate`, `location`) VALUES
(4, 'company', 'company', '9876543210', 'company@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 1, 1, '2020-12-01 18:30:07', '2020-12-17 14:40:02', 'Hyderabad');

-- --------------------------------------------------------

--
-- Table structure for table `course_branch`
--

CREATE TABLE `course_branch` (
  `id` int(11) NOT NULL,
  `qualification_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `course_branch`
--

INSERT INTO `course_branch` (`id`, `qualification_id`, `name`, `is_active`, `is_deleted`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, 1, 'Civil Engineering', 1, 0, 4, '2020-12-23 14:37:51', NULL, '2020-12-23 14:37:51'),
(2, 1, 'Computer Science & Engineering (CSE)', 1, 0, 4, '2020-12-23 14:37:51', NULL, '2020-12-23 14:37:51'),
(3, 1, 'Electrical & Electronics Engineering (EEE)', 1, 0, 4, '2020-12-23 14:37:55', NULL, '2020-12-23 14:38:58'),
(4, 1, 'Electrical Engineering', 1, 0, 4, '2020-12-23 14:37:55', NULL, '2020-12-23 14:39:11'),
(5, 1, 'Electronics & Communication Engineering(ECE)', 1, 0, 4, '2020-12-23 14:38:11', NULL, '2020-12-23 14:39:21'),
(6, 1, 'Mechanical Engineering', 1, 0, 4, '2020-12-23 14:38:11', NULL, '2020-12-23 14:39:43'),
(7, 3, 'Accountancy', 1, 0, 4, '2020-12-23 14:38:19', NULL, '2020-12-23 14:40:40'),
(8, 3, 'Business Economics', 1, 0, 4, '2020-12-23 14:38:19', NULL, '2020-12-23 14:40:51'),
(9, 3, 'Economics', 1, 0, 4, '2020-12-23 14:38:25', NULL, '2020-12-23 14:41:02'),
(10, 5, 'Advanced Communication System', 1, 0, 4, '2020-12-23 14:38:25', NULL, '2020-12-23 14:42:07'),
(11, 5, 'Applied Electronics', 1, 0, 4, '2020-12-23 14:38:31', NULL, '2020-12-23 14:42:42'),
(12, 5, 'Electronics & Communication Engineering', 1, 0, 4, '2020-12-23 14:38:31', NULL, '2020-12-23 14:43:11');

-- --------------------------------------------------------

--
-- Table structure for table `hiring_process`
--

CREATE TABLE `hiring_process` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hiring_process`
--

INSERT INTO `hiring_process` (`id`, `name`, `is_active`, `is_deleted`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, 'Face to Face', 1, 0, 4, '2020-12-23 13:58:13', NULL, '2020-12-23 13:58:13'),
(2, 'Telephonic', 1, 0, 4, '2020-12-23 13:58:13', NULL, '2020-12-23 13:58:13'),
(3, 'Walk-In', 1, 0, 4, '2020-12-23 13:58:13', NULL, '2020-12-23 13:58:13'),
(4, 'Written-test', 1, 0, 4, '2020-12-23 13:58:13', NULL, '2020-12-23 13:58:13'),
(5, 'Group Discussion', 1, 0, 4, '2020-12-23 13:58:13', NULL, '2020-12-23 13:58:13');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` text NOT NULL,
  `qualification` text DEFAULT NULL,
  `branch` text NOT NULL,
  `location` text NOT NULL,
  `min_salary` varchar(250) DEFAULT NULL,
  `max_salary` varchar(250) DEFAULT NULL,
  `hiring_process` text DEFAULT NULL,
  `passout_year_from` year(4) DEFAULT NULL,
  `passout_year_to` year(4) DEFAULT NULL,
  `percentage_from` decimal(4,2) DEFAULT NULL,
  `cgpa_from` decimal(4,2) DEFAULT NULL,
  `skills` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `experience_from` int(11) DEFAULT NULL,
  `experience_to` int(11) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `description` text NOT NULL,
  `job_status` enum('Open','Paused','Closed') NOT NULL DEFAULT 'Open',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `job_course_branch`
--

CREATE TABLE `job_course_branch` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `course_branch_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `job_hiring_process`
--

CREATE TABLE `job_hiring_process` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `hiring_process_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `job_location`
--

CREATE TABLE `job_location` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `job_qualification`
--

CREATE TABLE `job_qualification` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `qualification_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `job_skills`
--

CREATE TABLE `job_skills` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `skills_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `name`, `is_active`, `is_deleted`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, 'Bangalore', 1, 0, 4, '2020-12-23 13:01:33', NULL, '2020-12-23 13:01:33'),
(2, 'Chennai', 1, 0, 4, '2020-12-23 13:40:29', NULL, '2020-12-23 13:40:29'),
(3, 'Delhi', 1, 0, 4, '2020-12-23 13:40:29', NULL, '2020-12-23 13:40:49'),
(4, 'Hyderabad', 1, 0, 4, '2020-12-23 13:40:39', NULL, '2020-12-23 13:41:13'),
(5, 'Pune', 1, 0, 4, '2020-12-23 13:40:39', NULL, '2020-12-23 13:41:18');

-- --------------------------------------------------------

--
-- Table structure for table `qualification`
--

CREATE TABLE `qualification` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `qualification`
--

INSERT INTO `qualification` (`id`, `name`, `is_active`, `is_deleted`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, 'B.E/B.Tech', 1, 0, 4, '2020-12-22 12:58:01', NULL, '2020-12-22 12:58:01'),
(2, 'B.Com', 1, 0, 4, '2020-12-22 12:58:01', NULL, '2020-12-22 12:58:01'),
(3, 'BA', 1, 0, 4, '2020-12-22 12:58:01', NULL, '2020-12-22 12:58:01'),
(4, 'BBA/BBM', 1, 0, 4, '2020-12-22 12:58:01', NULL, '2020-12-22 12:58:01'),
(5, 'M.Tech', 1, 0, 4, '2020-12-22 13:06:43', NULL, '2020-12-22 13:06:43'),
(6, 'M.Com', 1, 0, 4, '2020-12-22 13:06:43', NULL, '2020-12-22 13:06:43');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `category` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `screener_questions`
--

CREATE TABLE `screener_questions` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `min_experience` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `min_education` varchar(150) DEFAULT NULL,
  `relocation` enum('Yes','No') DEFAULT NULL,
  `preferred_shift` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `name`, `is_active`, `is_deleted`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, 'HTML', 1, 0, 4, '2020-12-23 14:22:33', NULL, '2020-12-23 14:22:33'),
(2, 'CSS', 1, 0, 4, '2020-12-23 14:22:33', NULL, '2020-12-23 14:22:33'),
(3, 'HTML5', 1, 0, 4, '2020-12-23 14:22:37', NULL, '2020-12-23 14:22:47'),
(4, 'CSS3', 1, 0, 4, '2020-12-23 14:22:37', NULL, '2020-12-23 14:22:53'),
(5, 'Bootstrap 3', 1, 0, 4, '2020-12-23 14:24:00', NULL, '2020-12-23 14:24:00'),
(6, 'Bootstrap 4', 1, 0, 4, '2020-12-23 14:24:00', NULL, '2020-12-23 14:24:00'),
(7, 'PHP', 1, 0, 4, '2020-12-23 14:24:03', NULL, '2020-12-23 14:24:12'),
(8, 'Jquery', 1, 0, 4, '2020-12-23 14:24:03', NULL, '2020-12-23 14:24:20');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `image` text DEFAULT NULL,
  `default_account_type` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE `user_type` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_type` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_otp`
--
ALTER TABLE `admin_otp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applied_candidates`
--
ALTER TABLE `applied_candidates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `college`
--
ALTER TABLE `college`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `college_brochure`
--
ALTER TABLE `college_brochure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `college_course`
--
ALTER TABLE `college_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `college_faculty`
--
ALTER TABLE `college_faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `college_placement`
--
ALTER TABLE `college_placement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_branch`
--
ALTER TABLE `course_branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hiring_process`
--
ALTER TABLE `hiring_process`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_course_branch`
--
ALTER TABLE `job_course_branch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jcb_job_id_fk` (`job_id`);

--
-- Indexes for table `job_hiring_process`
--
ALTER TABLE `job_hiring_process`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jhp_job_id_fk` (`job_id`);

--
-- Indexes for table `job_location`
--
ALTER TABLE `job_location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jl_job_id_fk` (`job_id`);

--
-- Indexes for table `job_qualification`
--
ALTER TABLE `job_qualification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jq_job_id_fk` (`job_id`) USING BTREE;

--
-- Indexes for table `job_skills`
--
ALTER TABLE `job_skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `js_job_id_fk` (`job_id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qualification`
--
ALTER TABLE `qualification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `screener_questions`
--
ALTER TABLE `screener_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `admin_otp`
--
ALTER TABLE `admin_otp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `applied_candidates`
--
ALTER TABLE `applied_candidates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `college`
--
ALTER TABLE `college`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `college_brochure`
--
ALTER TABLE `college_brochure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `college_course`
--
ALTER TABLE `college_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `college_faculty`
--
ALTER TABLE `college_faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `college_placement`
--
ALTER TABLE `college_placement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `course_branch`
--
ALTER TABLE `course_branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `hiring_process`
--
ALTER TABLE `hiring_process`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `job_course_branch`
--
ALTER TABLE `job_course_branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `job_hiring_process`
--
ALTER TABLE `job_hiring_process`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `job_location`
--
ALTER TABLE `job_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `job_qualification`
--
ALTER TABLE `job_qualification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `job_skills`
--
ALTER TABLE `job_skills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `qualification`
--
ALTER TABLE `qualification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `screener_questions`
--
ALTER TABLE `screener_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `job_course_branch`
--
ALTER TABLE `job_course_branch`
  ADD CONSTRAINT `jcb_job_id_fk` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `job_hiring_process`
--
ALTER TABLE `job_hiring_process`
  ADD CONSTRAINT `jhp_job_id_fk` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `job_location`
--
ALTER TABLE `job_location`
  ADD CONSTRAINT `jl_job_id_fk` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `job_qualification`
--
ALTER TABLE `job_qualification`
  ADD CONSTRAINT `fk_job_id` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `job_skills`
--
ALTER TABLE `job_skills`
  ADD CONSTRAINT `js_job_id_fk` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
