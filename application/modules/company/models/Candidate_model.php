<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Candidate_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    
    public function getAllAppliedCandidates() {

        $query = "SELECT * FROM applied_candidates 
                    JOIN `admin` ON `admin`.`id` = `applied_candidates`.`user_id`";
        $query = $this->db->query($query);
        return $query->result();

    }

    public function getCandidateDetails($id) {

        $query = "SELECT * FROM applied_candidates 
                    JOIN `admin` ON `admin`.`id` = `applied_candidates`.`user_id`
                    JOIN `jobs` ON `jobs`.`id` = `applied_candidates`.`job_id`
                    WHERE applied_candidates.user_id = '".$id."'";
        $result = $this->db->query($query);
        $result = $result->row_array();
        return $result;

    }
    
}