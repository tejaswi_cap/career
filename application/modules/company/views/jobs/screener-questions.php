<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Screener questions</h4> 
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url(); ?>">Home</a></li>
                <li><a href="<?php echo user_url; ?>dashboard">Dashboard</a></li>
                <li class="active">Screener Questions</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- Blog-component -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Applicant qualifications</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <h4>Ask applicants about their qualifications</h4>
                        <p>Specify requirements that you want applicants to meet below. Jobseekers will be asked to confirm these when they apply for your job.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Experience
                    <div class="panel-action"> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a></div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        Minimun of <input type="text" id="min_experience" name="min_experience"> 
                        of <input type="text" id="type" name="type"> experience
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Education
                    <div class="panel-action"> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a></div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        Minimum level of education <select name="min_education" id="min_education">
                            <option value=""></option>
                            <option value="1">Higher Secondary(12th pass)</option>
                            <option value="2">Graduate</option>
                            <option value="3">Post Graduate</option>
                        </select> 
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Location
                    <div class="panel-action"> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a></div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        Job relocation <div class="radio-list">
                            <label class="radio-inline p-0">
                                <div class="radio radio-info">
                                    <input type="radio" name="relocation" id="radio1" value="yes">
                                    <label for="radio1">Yes</label>
                                </div>
                            </label>
                            <label class="radio-inline">
                                <div class="radio radio-info">
                                    <input type="radio" name="relocation" id="radio2" value="no">
                                    <label for="radio2">No </label>
                                </div>
                            </label>
                            </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Shift availability:
                    <div class="panel-action"> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a></div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        Avalilable to work the following shifts <div class="radio-list">
                            <label class="radio-inline p-0">
                                <div class="radio radio-info">
                                    <input type="radio" name="preferred_shift" id="radio1" value="day_shift">
                                    <label for="radio1">Day</label>
                                </div>
                            </label>
                            <label class="radio-inline">
                                <div class="radio radio-info">
                                    <input type="radio" name="preferred_shift" id="radio2" value="night_shift">
                                    <label for="radio2">Night </label>
                                </div>
                            </label>
                            </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>    
</div>