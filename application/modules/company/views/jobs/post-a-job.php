<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php if($job['id'] == '') echo 'Post a job'; else echo 'Edit Job';?></h4> 
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url(); ?>">Home</a></li>
                <li><a href="<?php echo user_url; ?>dashboard">Dashboard</a></li>
                <li class="active"><?php if($job['id'] == '') echo 'Post a job'; else echo 'Edit Job';?></li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- Blog-component -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading"><?php if($job['id'] == '') echo 'Post a job'; else echo 'Edit Job';?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form class="" role="form" id="postAJobForm" enctype="multipart/form-data">
                            <div class="form-body">
                                <h3 class="box-title">Tell us about your job</h3>
                                <hr>
                                <input type="hidden" id="id" name="id" value="<?php echo $job['id']; ?>">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label>Job Title</label>
                                            <input type="text" id="title" name="title" class="form-control" required="required" data-error="Job Title required" placeholder="Software Engineer/ Marketing executive/ Sales executive etc." value="<?php echo $job['title']; ?>"> 
                                            <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label>Job Type</label>
                                            <select class="form-control" name="type" id="type" required="required" data-error="Job Type required">
                                                <option value="">Choose</option>
                                                <option value="Full Time" <?php if($job['type'] == "Full Time"){ echo 'selected';} ?>>Full Time</option>
                                                <option value="Part Time" <?php if($job['type'] == "Part Time"){ echo 'selected';} ?>>Part Time</option>
                                                <option value="Internship" <?php if($job['type'] == "Internship"){ echo 'selected';} ?>>Internship</option>
                                                <option value="Apprenticeship" <?php if($job['type'] == "Apprenticeship"){ echo 'selected';} ?>>Apprenticeship</option>
                                            </select> <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label>Qualification / Eligibility</label>
                                            <select class="select2  select2-multiple" name="qualification[]" required="required" id="qualification" multiple="multiple" data-placeholder="Choose" required="required" data-error="Qualification required">
                                                <?php foreach($qualifications as $qualification) {?>
                                                    <option value="<?php echo $qualification->id?>" <?php if($job['qualification'] != '') if(in_array($qualification->id,json_decode($job['qualification'],true))){ echo 'selected';} ?>><?php echo $qualification->name?></option>
                                                <?php }?>
                                            </select> <span class="help-block with-errors"></span>  </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Select Course Branch </label>
                                            <?php 
                                                $qualification = array();
                                                foreach($course_branch as $branch)
                                                { 
                                                    $qualification[$branch->qualification][] = $branch;
                                                }
                                            ?>
                                            <select class="select2 m-b-10 select2-multiple" name="branch[]" id="branch" multiple="multiple" data-placeholder="Choose">
                                                <?php foreach($qualification as $key => $branch) {?>
                                                    <optgroup label="<?php echo $key?>">
                                                        <?php foreach($branch as $branch) {?>
                                                            <option value="<?php echo $branch->id?>" <?php if($job['branch'] != '') if(in_array($branch->id,json_decode($job['branch'],true))){ echo 'selected';} ?>><?php echo $branch->name?></option>
                                                        <?php }?>
                                                    </optgroup>
                                                <?php }?>
                                            </select> <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label>Job Location </label>
                                            <select class="select2 m-b-10 select2-multiple" name="location[]" id="location" required="required" multiple="multiple" data-placeholder="Choose" required="required" data-error="Location required">
                                            <?php foreach($locations as $location) {?>
                                                    <option value="<?php echo $location->id?>" <?php if($job['location'] != '') if(in_array($location->id,json_decode($job['location'],true))){ echo 'selected';} ?>><?php echo $location->name?></option>
                                                <?php }?>
                                            </select> <span class="help-block with-errors"></span> </div>                                               
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                            <label class="control-label">Yearly Salary(Min)</label>
                                            <input type="number" id="min_salary" name="min_salary" class="form-control" placeholder="" value="<?php echo $job['min_salary']; ?>"> 
                                            <span class="help-block with-errors"></span> </div>
                                        </div>
                                        <div class="col-md-3 col-md-offset-1 col-xs-12">																	
                                            <label class="text-center">-</label>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Yearly Salary(Max)</label>
                                                <input type="number" id="max_salary" name="max_salary" class="form-control" placeholder="" value="<?php echo $job['max_salary']; ?>"> 
                                                <span class="help-block with-errors"></span> </div>
                                            </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Hiring Process</label>
                                                <?php foreach($hiring_process as $hiring_process) {?>
                                                    <div class="checkbox">
                                                        <input id="checkbox<?php echo $hiring_process->id?>" name="hiring_process[]" value="<?php echo $hiring_process->id?>" type="checkbox" <?php if($job['hiring_process'] != '') if(in_array($hiring_process->id,json_decode($job['hiring_process'],true))){ echo 'checked';} ?>>
                                                        <label for="checkbox<?php echo $hiring_process->id?>"> <?php echo $hiring_process->name?> </label>
                                                    </div>
                                                <?php }?>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="form-group col-md-6 col-xs-12">
                                        <div class="row col-md-12">
                                            <label class="control-label col-md-6">Year Of Passing<span> </span></label>
                                        </div>
                                        <div class="row col-md-12">
                                            <div class="col-md-4 col-sm-3">
                                                <label class="text-center">From</label>
                                                <select class=" form-control" name="passout_year_from" id="passout_year_from">			
                                                    <option value="">Any</option>
                                                    <?php 
                                                        for($i = date('Y') + 2 ; $i > 1950; $i--){
                                                            echo '<option value="'.$i.'"'. ($job['passout_year_from'] == $i ? ' selected = "selected"':'').'>'.$i.'</option>';
                                                        }
                                                    ?> 
                                                </select>
                                                
                                            </div>
                                            <div class="col-md-3 col-md-offset-1 col-xs-12">																	
                                                <label class="text-center">-</label>
                                            </div>
                                            <div class="col-md-4 col-sm-3">
                                                <label class="text-center">To</label>
                                                <select class=" form-control" name="passout_year_to" id="passout_year_to">
                                                <option value="">Any</option>
                                                    <?php 
                                                        for($i = date('Y') + 2 ; $i > 1950; $i--){
                                                            echo '<option value="'.$i.'"'. ($job['passout_year_from'] == $i ? ' selected = "selected"':'').'>'.$i.'</option>';
                                                        }
                                                    ?> 
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>    
                                    <div class="form-group col-md-6 col-xs-12">
                                        <div class="row col-md-12">
                                            <label class="control-label col-md-12" >Highest Qualification Marks</label>
                                        </div>
                                        <div class="row col-md-12">
                                            <div class="col-md-4 col-xs-12">
                                                <label class="text-center">Percentage</label>
                                                <input type="number" max="100" maxlength="5" placeholder=" %" name="percentage_from" id="percentage_from" class="form-control input-sm Percentage" value="<?php echo $job['percentage_from']; ?>">
                                            </div>
                                            <div class="col-md-3 col-md-offset-1 col-xs-12 col-xs-offset-5" style="margin-top: 15px;">
                                            or
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <label class="text-center">CGPA</label>
                                            <input type="number" max="10" maxlength="4" placeholder=" /10" name="cgpa_from" id="cgpa_from" class="form-control input-sm CGPA" value="<?php echo $job['cgpa_from']; ?>">
                                            </div>
                                        </div>
                                    </div>            
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="form-group col-md-6 col-xs-12">
                                        <div class="row col-md-12">
                                            <div class="form-group">
                                                <label>Skills</label>
                                                <select class="select2 m-b-10 select2-multiple" name="skills[]" id="skills" multiple="multiple" data-placeholder="Choose">
                                                <?php foreach($skills as $skills) {?>
                                                    <option value="<?php echo $skills->id?>" <?php if($job['skills'] != '') if(in_array($skills->id,json_decode($job['skills'],true))){ echo 'selected';} ?>><?php echo $skills->name?></option>
                                                <?php }?>
                                                </select> <span class="help-block with-errors"></span> </div>
                                                
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <div class="row col-md-12">
                                            <label class="control-label col-md-6">Experience</label>
                                        </div>
                                        <div class="row col-md-12">
                                            <div class="col-md-4 col-sm-3">
                                                <label class="text-center">From</label>
                                                <select id="experience_from" name="experience_from" class="form-control input-sm From" >
                                                    <option value="">Any</option>
                                                    <option value="0" <?php if($job['experience_from'] == "0"){ echo 'selected';} ?>>0 (Fresher)</option>
                                                    <option value="06" <?php if($job['experience_from'] == "06"){ echo 'selected';} ?>>06 Months</option>
                                                    <option value="12" <?php if($job['experience_from'] == "12"){ echo 'selected';} ?>>1 Year</option>
                                                    <option value="18" <?php if($job['experience_from'] == "18"){ echo 'selected';} ?>>1.5 Years</option>
                                                    <option value="24" <?php if($job['experience_from'] == "24"){ echo 'selected';} ?>>2 Years</option>
                                                    <option value="30" <?php if($job['experience_from'] == "30"){ echo 'selected';} ?>>2.5 Years</option>
                                                    <option value="36" <?php if($job['experience_from'] == "36"){ echo 'selected';} ?>>3 years</option>
                                                    <option value="500" <?php if($job['experience_from'] == "500"){ echo 'selected';} ?>>3+ Years</option>
                                                </select>
                                                
                                            </div>
                                            <div class="col-md-3 col-md-offset-1 col-xs-12">																	
                                                <label class="text-center">-</label>
                                            </div>
                                            <div class="col-md-4 col-sm-3">
                                                <label class="text-center">To</label>
                                                <select id="experience_to" name="experience_to" class="form-control input-sm To" >
                                                    <option value="">Any</option>
                                                    <option value="0" <?php if($job['experience_to'] == "0"){ echo 'selected';} ?>>0 (Fresher)</option>
                                                    <option value="06" <?php if($job['experience_to'] == "06"){ echo 'selected';} ?>>06 Months</option>
                                                    <option value="12" <?php if($job['experience_to'] == "12"){ echo 'selected';} ?>>1 Year</option>
                                                    <option value="18" <?php if($job['experience_to'] == "18"){ echo 'selected';} ?>>1.5 Years</option>
                                                    <option value="24" <?php if($job['experience_to'] == "24"){ echo 'selected';} ?>>2 Years</option>
                                                    <option value="30" <?php if($job['experience_to'] == "30"){ echo 'selected';} ?>>2.5 Years</option>
                                                    <option value="36" <?php if($job['experience_to'] == "36"){ echo 'selected';} ?>>3 years</option>
                                                    <option value="500" <?php if($job['experience_to'] == "500"){ echo 'selected';} ?>>3+ Years</option>
                                                    </select>
                                                
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Gender</label>
                                            <div class="radio-list">
                                                <label class="radio-inline p-0">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="gender" id="male" value="male" <?php if($job['gender'] == "male"){ echo 'checked';} ?>>
                                                        <label for="male">Male</label>
                                                    </div>
                                                </label>
                                                <label class="radio-inline">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="gender" id="female" value="female" <?php if($job['gender'] == "female"){ echo 'checked';} ?>>
                                                        <label for="female">Female </label>
                                                    </div>
                                                </label>
                                                <label class="radio-inline">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="gender" id="both" value="both" <?php if($job['gender'] == "both"){ echo 'checked';} ?>>
                                                        <label for="both">Both </label>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12 required">
                                        <label >Job Description</label>
                                        <!-- <textarea id="mymce" name="description" required="required" data-error="Description required"></textarea> -->
                                        <textarea class="form-control" rows="15" cols="50" id="description" name="description" required="required" data-error="Description required"><?php echo $job['description']; ?></textarea>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success save-category"> <i class="fa fa-check"></i> Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="col-sm-8 col-md-4">
            <div class="white-box">
                <h3 class="box-title">Why use Post-n-Hire ?</h3>
                <hr>
                <div class="">
                    <ul class="list-group" style="padding: 4px 10px 0 26px;margin-bottom: 5px;">
                        <li class="right-boxes" style="border-top: none;margin-top: 2%;">
                            User-friendly Job posting &amp; response management
                        </li>
                        <li class="right-boxes">Automated Job alerts to get you responses faster</li>
                        <li class="right-boxes">Get responses near your office location</li>
                        <li class="right-boxes">Attach Questionnaire to a Job</li>
                        <li class="right-boxes" style="border-bottom: none;">Get Job responses alert on your mail</li>										
                    </ul>
                </div>
            </div>
        </div> -->
    </div>
</div>

<input type="hidden" id="currentPath" value="<?php echo site_url(); ?>company/job/">
