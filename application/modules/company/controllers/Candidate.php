<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Candidate extends CI_Controller {
    
    //set Header page
    public $headerPage = '../../views/admin/header';
    
    //set footer page
    public $footerPage = '../../views/admin/footer';
    
    public function __Construct() {

        parent::__Construct();
        $this->load->model('candidate_model', 'my_model');
        if ($this->session->userdata('adminId') == '') {
            redirect(base_url());
        }

    }

    public function index() {

        $data['applied_candidates'] = $this->my_model->getAllAppliedCandidates();
        $this->load->view($this->headerPage, $data);
        $this->load->view("candidates/candidates", $data);
        $this->load->view($this->footerPage, $data);

    }


    public function candidate($id) {
        $data['candidate'] = $this->my_model->getCandidateDetails($id);
        $this->load->view($this->headerPage, $data);
        $this->load->view("candidates/candidate", $data);
        $this->load->view($this->footerPage, $data);

    }

    public function saveJob() {

        $result = $this->my_model->editJob();
        echo json_encode($result);

    }

    public function screenerQuestions($id) {

        $data['job'] = $this->my_model->getjobDetails($id);
        $data['job_id'] = $id;
        if($data['job'] != 0){
            $this->load->view($this->headerPage, $data);
            $this->load->view("jobs/screener-questions", $data);
            $this->load->view($this->footerPage, $data);
        }

    }

    public function updateJob($id) {
        $data['job'] = $this->my_model->getjobDetails($id);
        $this->load->view($this->headerPage, $data);
        $this->load->view("jobs/post-a-job", $data);
        $this->load->view($this->footerPage, $data);

    }
}
