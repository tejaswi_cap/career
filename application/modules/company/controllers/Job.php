<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Job extends CI_Controller {
    
    //set Header page
    public $headerPage = '../../views/admin/header';
    
    //set footer page
    public $footerPage = '../../views/admin/footer';
    
    public function __Construct() {

        parent::__Construct();
        $this->load->model('job_model', 'my_model');
        $this->load->model('admin/qualification_model','qualification_model');
        $this->load->model('admin/location_model','location_model');
        $this->load->model('admin/hiring_process_model','hiring_process_model');
        $this->load->model('admin/skills_model','skills_model');
        $this->load->model('admin/course_branch_model','course_branch_model');
        if ($this->session->userdata('adminId') == '') {
            redirect(base_url());
        }

    }

    public function index() {

        $data['jobs'] = $this->my_model->getAllJobs();
        $this->load->view($this->headerPage, $data);
        $this->load->view("jobs/jobs", $data);
        $this->load->view($this->footerPage, $data);

    }


    public function postJob() {
        $data['job'] = array(
    	    'id' => set_value('id'),
    	    'title' => set_value('title'),
    	    'type' => set_value('type'),
    	    'qualification' => set_value('qualification',''),
    	    'branch' => set_value('branch'),
    	    'location' => set_value('location'),
    	    'min_salary' => set_value('min_salary'),
    	    'max_salary' => set_value('max_salary'),
    	    'hiring_process' => set_value('hiring_process'),
    	    'passout_year_from' => set_value('passout_year_from'),
    	    'passout_year_to' => set_value('passout_year_to'),
    	    'percentage_from' => set_value('percentage_from'),
    	    'cgpa_from' => set_value('cgpa_from'),
    	    'skills' => set_value('skills'),
    	    'experience_from' => set_value('experience_from'),
    	    'experience_to' => set_value('experience_to'),
    	    'gender' => set_value('gender'),
    	    'description' => set_value('description'),
    	    'job_status' => set_value('job_status'),

	    );
        $data['qualifications'] = $this->qualification_model->getAllQualifications();
        $data['locations'] = $this->location_model->getAllLocations();
        $data['hiring_process'] = $this->hiring_process_model->getAllHiringProcess();
        $data['skills'] = $this->skills_model->getAllSkills();
        $data['course_branch'] = $this->course_branch_model->getAllCourseBranches();
        $this->load->view($this->headerPage, $data);
        $this->load->view("jobs/post-a-job", $data);
        $this->load->view($this->footerPage, $data);

    }

    public function saveJob() {

        $result = $this->my_model->editJob();
        echo json_encode($result);

    }

    public function screenerQuestions($id) {

        $data['job'] = $this->my_model->getjobDetails($id);
        $data['job_id'] = $id;
        if($data['job'] != 0){
            $this->load->view($this->headerPage, $data);
            $this->load->view("jobs/screener-questions", $data);
            $this->load->view($this->footerPage, $data);
        }

    }

    public function updateJob($id) {
        $data['job'] = $this->my_model->getjobDetails($id);
        $data['qualifications'] = $this->qualification_model->getAllQualifications();
        $data['locations'] = $this->location_model->getAllLocations();
        $data['hiring_process'] = $this->hiring_process_model->getAllHiringProcess();
        $data['skills'] = $this->skills_model->getAllSkills();
        $data['course_branch'] = $this->course_branch_model->getAllCourseBranches();
        $this->load->view($this->headerPage, $data);
        $this->load->view("jobs/post-a-job", $data);
        $this->load->view($this->footerPage, $data);

    }
}
