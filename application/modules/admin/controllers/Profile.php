<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller {
    
    //set Header page
    public $headerPage = '../../views/admin/header';
    
    //set footer page
    public $footerPage = '../../views/admin/footer';
    
    public function __Construct() {

        parent::__Construct();
        $this->load->model('profile_model', 'my_model');
        if ($this->session->userdata('adminId') == '') {
            redirect(base_url());
        }

    }

    public function index() {

        $data['user'] = $this->my_model->getuserDetails();
        $this->load->view($this->headerPage, $data);
        $this->load->view("profile/profile", $data);
        $this->load->view("profile/modal");
        $this->load->view($this->footerPage, $data);

    }

    public function profileimageupdate()
    {
        $userId = $this->userId;
        if ($_FILES["profile"]["name"]!="") {
            $extension = explode(".", $_FILES["profile"]["name"]);
            $extension = end($extension);
            $image=time().'_'.$userId.".".$extension;
            if (!file_exists('images/profile/')) {
                mkdir('images/profile/');
            }
            $path="./images/profile/";
            $this->load->library('upload');
            $config['upload_path']          = $path;
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $this->upload->initialize($config);
            $_FILES['userfile']['name']=$image;
            $_FILES['userfile']['tmp_name']=$_FILES['profile']['tmp_name'];
            $_FILES['userfile']['size']=$_FILES['profile']['size'];
            if ($this->upload->do_upload()) {
                $imageName = $image;
            } else {
                $imageName = "";
            }
        } else {
            $imageName = "";
        }
        $result = $this->my_model->setUploadProfile($imageName);
        echo json_encode($result);
        
    }


    public function removeprofileimage()
    {
        
        $result = $this->my_model->setDeleteProfilePic();
        echo json_encode($result);
        
    }
        
            
    public function editpassword()
    {
        $response = array();
        $userId = $this->session->userdata('adminId');
        $adminDetails = $this->datatable_model->getsingleDetails($userId, "admin", 'id');
        extract($this->input->post());
        $oldPassword = $adminDetails['password'];
        if ($oldPassword == md5($oldpassword)) {
            
            $result = $this->my_model->editPassword($userId);
            $response = array("response" => 1 , "message" => "Your Password Updated Successfully.");
            
        } else {
            $response = array(
                "response" => "Failure" , 
                "message" => "New and Confirm Passwords Didn't Matched."
            );
        }
        echo json_encode($response);
    }

    public function removeimage()
    {
        $id = $this->input->post('id');
        $table = $this->input->post('table');
        $column = $this->input->post('column');
        $result = $this->my_model->removeImage($table,$column,$id);
        echo json_encode($result);
    }

    public function editprofile() {

        $result = $this->my_model->editProfile();
        echo json_encode($result);

    }

    public function editemail() {

        $result = $this->my_model->editEmail();
        echo json_encode($result);
    }

    public function editmobile() {

        $result = $this->my_model->editMobile();
        echo json_encode($result);
    }

    public function sendotp() {

        $email = $this->session->userdata('adminEmail');
        $response = $this->my_model->sendOtpEmail();
        if($response['status'] == "Success") {
            $otp = $response['otp'];
            $message = $this->load->view("email/otp", "", true);
            $message = str_replace("{OTP}", $otp, $message);
            $content = array(
                "subject" => "Update Email Confirmation",
                "message" => $message
            );
            $this->mail_model->send($email, $content);
            unset($response['otp']);
        }
        echo json_encode($response);

    }

}
