<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Coursecategories extends CI_Controller {
    
    //set Header page
    public $headerPage = '../../views/admin/header';
    
    //set footer page
    public $footerPage = '../../views/admin/footer';
    
    public function __Construct() {

        parent::__Construct();
        $this->load->database();

        if ($this->session->userdata('adminId') == '') {
            redirect(base_url());
        }

    }

	public function index()
	{
        $crud = new grocery_CRUD();
        $created_by = $this->session->userdata('adminId');
		$crud->set_theme('bootstrap');

        $crud->set_table('course_categories');
        $crud->columns('name','status');
        $crud->fields('name','status');
        $crud->add_action('Change Status', '', 'admin/coursecategories/change_status');
        $crud->field_type('status','dropdown',
        array('1' => 'active', '0' => 'inactive'));
        $crud->set_relation('created_by','admin','name');
        $crud->set_relation('updated_by','admin','name');
        $crud->field_type('created_by','invisible');
        $crud->field_type('created_date','invisible');
        $crud->field_type('updated_by','invisible');
        $crud->field_type('updated_date','invisible');

        $crud->callback_read_field('status', array($this, 'callback_status'));

               
        $crud->callback_before_insert(function ($post_array)  {
            if (empty($post_array['created_by'])) {
                $post_array['created_by'] = $this->session->userdata('adminId');
            }
            return $post_array;
        });

        $crud->callback_before_update(function ($post_array)  {
            if (empty($post_array['updated_by'])) {
                $post_array['updated_by'] = $this->session->userdata('adminId');
            }
            return $post_array;
        });
        
        $output = $crud->render();

        $this->_example_output($output);
	}

    function _example_output($output = null)
 
    {

        $output = (array)$output;
        $output['title'] = "Course Categories";
        $this->load->view($this->headerPage,$output);
        $this->load->view('grocery_template.php',$output);    
        $this->load->view($this->footerPage,$output);
    }  

    function change_status()
    {
        $result = array();
        
        $link = $_SERVER['REQUEST_URI'];
        $link_array = explode('/',$link);
        $id = end($link_array);

        //SAVE TO DATABASE
        $query = "SELECT * FROM course_categories WHERE id = '".$id."'";
        $result = $this->db->query($query);
        $count = $result->num_rows();
        $row = $result->row_array();
        $data = array(
            'updated_by' => $this->session->userdata('adminId')
        );
        if($row['status'] == 1){
            $data['status'] = 0;
        }elseif($row['status'] == 0){
            $data['status'] = 1;
        }
        if($count > 0){
            $this->db->where('id',$id);
            $this->db->update('course_categories', $data);
        }
        redirect(admin_url.'coursecategories');
    }

    function callback_status($value, $row)
    {
        if($value == 1){
            return 'active';
        }else{
            return 'inactive';
        }
    }
}
