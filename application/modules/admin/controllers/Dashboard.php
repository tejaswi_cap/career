<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public $headerPage = '../../views/admin/header';
    //set footer page
    public $footerPage = '../../views/admin/footer';

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
        if ($this->session->userdata('adminId') == '') {
            redirect(admin_url);
        }
    }
    
    
    public function index() {

        $data = array();
        $this->load->view($this->headerPage,$data);
        $this->load->view('index',$data);
        $this->load->view($this->footerPage,$data);
    }
    
    
}
