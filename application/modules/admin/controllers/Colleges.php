<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Colleges extends CI_Controller {
    
    //set Header page
    public $headerPage = '../../views/admin/header';
    
    //set footer page
    public $footerPage = '../../views/admin/footer';
    
    public function __Construct() {

        parent::__Construct();
        $this->load->database();

        if ($this->session->userdata('adminId') == '') {
            redirect(base_url());
        }

    }

	public function index()
	{
        $crud = new grocery_CRUD();
        $created_by = $this->session->userdata('adminId');
		$crud->set_theme('bootstrap');
        $crud->set_table('college');
        $crud->columns('college_type_id','name','location_id','sub_location');
        $crud->fields('college_type_id','name','location_id','sub_location');
        $crud->field_type('autonomous','dropdown',
            array('1' => 'yes', '0' => 'no'));
            $crud->field_type('is_active','dropdown',
            array('1' => 'yes', '0' => 'no'));
        $crud->field_type('ownership','enum',array('Public/Government','Private'));
        $crud->required_fields('college_type_id','name');

        $crud->set_relation('college_type_id','course_categories','name',array('status'=> 1));
        $crud->set_relation('location_id','location','name');
        $crud->set_relation('created_by','admin','name');
        $crud->set_relation('updated_by','admin','name');
        $crud->display_as('college_type_id','College Type');
        $crud->display_as('location_id','Location');

        if (!is_dir('uploads/colleges/logo/')) {
            mkdir('uploads/colleges/logo/', 0777, true);
        }
        $crud->set_field_upload('logo','uploads/colleges/logo');
        $crud->callback_before_upload(array($this,'example_callback_before_upload'));

        $crud->field_type('logo_path','invisible');
        $crud->field_type('is_deleted','invisible');
        $crud->field_type('created_date','invisible');
        $crud->field_type('created_by','invisible');
        $crud->field_type('updated_date','invisible');
        $crud->field_type('updated_by','invisible');

        $crud->callback_before_insert(function ($post_array)  {
            if (!empty($post_array['logo'])) {
                $post_array['logo_path'] = base_url().'uploads/colleges/logo/'.$post_array['logo'];
            }
            if (empty($post_array['created_by'])) {
                $post_array['created_by'] = $this->session->userdata('adminId');
            }
            return $post_array;
        });

        $crud->callback_before_update(function ($post_array)  {
            if (!empty($post_array['logo'])) {
                $post_array['logo_path'] = base_url().'uploads/colleges/logo/'.$post_array['logo'];
            }
            if (empty($post_array['updated_by'])) {
                $post_array['updated_by'] = $this->session->userdata('adminId');
            }
            return $post_array;
        });
        
        $output = $crud->render();

        $this->_example_output($output);
	}

    function _example_output($output = null)
 
    {

        $output = (array)$output;
        $output['title'] = "Colleges";
        // var_dump($output);die;
        $this->load->view($this->headerPage,$output);
        $this->load->view('grocery_template.php',$output);    
        $this->load->view($this->footerPage,$output);
    }  

    function example_callback_before_upload($files_to_upload,$field_info)
    {
    
        if(is_dir($field_info->upload_path))
        {
            return true;
        }
        else
        {
            return 'I am sorry but it seems that the folder that you are trying to upload doesn\'t exist.';    
        }
    
    }

    function quick_add_save()
    {
        $result = array();
        //POST ITEMS
        $name = $_POST['name'];

        //SAVE TO DATABASE
        $data = array(
            'name' => $name,
            'status' => 0,
            'created_by' => $this->session->userdata('adminId')
        );
        $query = "SELECT * FROM course_categories WHERE name = '".$name."'";
        $result = $this->db->query($query);
        $count = $result->num_rows();
        if($count == 0){
            $this->db->insert('course_categories', $data);
            $result = array(
                'status' => 'success',
                'id' => $this->db->insert_id(),
                'name' => $name
            );
        }else{
            $result = array(
                'status' => 'error',
                'message' => 'Cant add name that already exists'
            );
        }

        echo json_encode($result); 
    }

}
