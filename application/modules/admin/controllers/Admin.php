<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
        $this->load->model("admin_model", "my_model");
        // error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
        // error_reporting(0);
        // ini_set('display_errors', 'off');
    }
    
    
    public function index() {
        if($this->session->userdata('adminId') == "") {
            $this->load->view('login');
        } else {
            redirect(site_url()."admin/dashboard");
        }

    }

    public function login() {

        $response = $this->my_model->getUserLogin();
        echo json_encode($response);
    }

    public function logout() {

        $user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            $this->session->unset_userdata($key);
        }
        $this->session->unset_userdata('adminId');
        $this->session->unset_userdata('adminName');
        $this->session->unset_userdata('adminEmail');
        $this->session->unset_userdata('adminMobile');
        $this->session->unset_userdata('adminImage');
        $this->session->unset_userdata('adminRole');
        $this->session->sess_destroy();

        redirect(admin_url, 'refresh');

    }

    public function resetpassword() {

        extract($this->input->post());
        $response = $this->my_model->resetPassword();
        if($response['status'] == "Success") {
            $password = $response['password'];
            $message = $this->load->view("email/resetpassword", "", true);
            $message = str_replace("{NEWPASSWORD}", $password, $message);
            $content = array(
                "subject" => "Reset Password",
                "message" => $message
            );
            unset($response['password']);
            $this->mail_model->send($email, $content);
        }
        echo json_encode($response);

    }
    
    
}
