<!DOCTYPE html>  
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png"> -->
        <title>Admin</title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo dashboard_assets; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="<?php echo dashboard_assets; ?>css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<?php echo dashboard_assets; ?>css/style.css" rel="stylesheet">
        <!-- color CSS -->
        <link href="<?php echo dashboard_assets; ?>css/colors/blue.css" id="theme"  rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="<?php echo dashboard_assets; ?>bootstrap/dist/css/bootstrap.min.css">
    
        <!-- Menu CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>theme/bower_components/sidebar-nav/dist/sidebar-nav.min.css">
        <!-- toast CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>theme/bower_components/toastr/build/toastr.css">
        <!-- Sweet Alert CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>theme/bower_components/sweetalert/sweetalert.css">
        <!-- morris CSS -->
        <link href="<?php echo site_url(); ?>theme/bower_components/morrisjs/morris.css" rel="stylesheet" type="text/css">
        <!-- chartist CSS -->
        <link href="<?php echo site_url(); ?>theme/bower_components/chartist-js/dist/chartist.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo site_url(); ?>theme/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="<?php echo site_url(); ?>theme/bower_components/datepicker/datepicker3.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <!-- Calendar CSS -->
        <link href="<?php echo site_url(); ?>theme/bower_components/calendar/dist/fullcalendar.css" rel="stylesheet"  type="text/css"/>
        <link href="<?php echo site_url(); ?>theme/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
        <link href="<?php echo site_url(); ?>theme/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        
        <link href="<?php echo site_url(); ?>theme/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
        <!-- Page plugins css -->
        <link href="<?php echo site_url(); ?>theme/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">

        <!-- Typehead CSS -->
        <link href="<?php echo site_url(); ?>theme/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">

        <!-- Select picker -->


        <link href="<?php echo site_url(); ?>theme/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url(); ?>theme/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
        <link href="<?php echo site_url(); ?>theme/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />

        <link href="<?php echo site_url(); ?>theme/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />


        <link rel="stylesheet" href="<?php echo site_url(); ?>theme/bower_components/dropify/dist/css/dropify.min.css">

        <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>theme/bower_components/fancybox/ekko-lightbox.min.css" />
        <!-- animation CSS -->
        <link href="<?php echo dashboard_assets; ?>css/animate.css" rel="stylesheet" type="text/css">
        <!-- Custom CSS -->
        <link href="<?php echo dashboard_assets; ?>css/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo dashboard_assets; ?>css/responsive.css" rel="stylesheet" type="text/css">
        <!-- color CSS -->
        <link href="<?php echo dashboard_assets; ?>css/colors/megna.css" id="theme" rel="stylesheet" type="text/css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <section id="wrapper" class="login-register">
            <div class="login-box login-sidebar">
                <div class="white-box">
                    <!-- <a href="javascript:void(0)" class="text-center db"><img src="<?php echo dashboard_assets; ?>images/logo.png" alt="Home" /></a> -->
                    <form class="form-horizontal new-lg-form" id="loginForm">
                        <div class="formAlertDiv">
                            <div class="alert alert-warning alert-dismissable m-t-10 dn">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <i class="fa fa-spinner fa-spin"></i> Please wait while we processing your request.
                            </div>
                        </div>
                        <div class="form-group  m-t-20">
                            <div class="col-xs-12">
                                <label>Email Address</label>
                                <input class="form-control" type="email" name="email" id="email" required placeholder="Email" data-error="Email id required">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label>Password</label>
                                <input class="form-control" type="password" name="password" id="password" required placeholder="Password" data-error="Password required">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> 
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light" type="submit">Log In</button>
                            </div>
                        </div>
                    </form>
                    <form class="form-horizontal dn" id="forgotPasswordForm">
                        <div class="formAlertDiv">
                            <div class="alert alert-warning alert-dismissable m-t-10 dn">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <i class="fa fa-spinner fa-spin"></i> Please wait while we processing..
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <h3>Recover Password</h3>
                                <p class="text-muted">Enter your email and password will be sent to you! </p>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <label>Email Address</label>
                                <input class="form-control" name="email" id="email" type="email" required="" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <a href="javascript:void(0)" id="to-login" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Login</a> 
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light" type="submit">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <input type="hidden" id="currentPath" value="<?php echo site_url(); ?>admin/">
        <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo dashboard_assets; ?>bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo site_url(); ?>theme/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?php echo dashboard_assets; ?>js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <!-- <script src="<?php echo dashboard_assets; ?>js/waves.js"></script> -->
    <!--Counter js -->
    <script src="<?php echo site_url(); ?>theme/bower_components/waypoints/lib/jquery.waypoints.js"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/counterup/jquery.counterup.min.js"></script>
    <!--Morris JavaScript -->
    <script src="<?php echo site_url(); ?>theme/bower_components/raphael/raphael-min.js"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/morrisjs/morris.js"></script>
    <!-- Animated skill bar -->
    <!-- <script src="<?php echo site_url(); ?>theme/bower_components/AnimatedSkillsDiagram/js/animated-bar.js"></script> -->
    <!-- chartist chart -->
    <script src="<?php echo site_url(); ?>theme/bower_components/chartist-js/dist/chartist.min.js"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <?php if($this->router->fetch_class() == "calendar" && $this->router->fetch_method() == "index") { ?>
    <!-- Calendar JavaScript -->
    <script src="<?php echo site_url(); ?>theme/bower_components/calendar/jquery-ui.min.js"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/moment/moment.js"></script>
    <script src='<?php echo site_url(); ?>theme/bower_components/calendar/dist/fullcalendar.min.js'></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/calendar/dist/jquery.fullcalendar.js"></script>
    <?php } ?>
    <script src="<?php echo site_url(); ?>theme/bower_components/datatables/jquery.dataTables.min.js"></script>

    <script src="<?php echo site_url(); ?>theme/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>

    <script src="<?php echo site_url(); ?>theme/bower_components/Chart.js/Chart.min.js"></script>

    <script src="<?php echo site_url(); ?>theme/bower_components/datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <script src="<?php echo site_url(); ?>theme/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <!--Style Switcher -->

    
    <script src="<?php echo site_url(); ?>theme/bower_components/switchery/dist/switchery.min.js"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

    <!-- Bootstrap Select Picker-->


    <script src="<?php echo site_url(); ?>theme/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>

    
    <!-- Clock Plugin JavaScript -->
    <script src="<?php echo site_url(); ?>theme/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>

    <!-- Toastr -->
    <script src="<?php echo site_url(); ?>theme/bower_components/toastr/toastr.js"></script>
    
    <!-- Sweet Alert -->
    <script src="<?php echo site_url(); ?>theme/bower_components/sweetalert/sweetalert.min.js"></script>
    <!-- Typehead Plugin JavaScript -->
    <script type="text/javascript" src="<?php echo site_url(); ?>theme/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>


    <!-- Dropzone Plugin JavaScript -->
    <script src="<?php echo site_url(); ?>theme/bower_components/dropzone-master/dist/dropzone.js"></script>
    <script type="text/javascript" src="<?php echo site_url(); ?>theme/bower_components/fancybox/ekko-lightbox.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo dashboard_assets; ?>js/custom.js"></script>
    <script src="<?php echo dashboard_assets; ?>js/mask.js"></script>
    <script src="<?php echo dashboard_assets; ?>js/validator.js"></script>
    <script src="<?php echo dashboard_assets; ?>js/main.js"></script>
    <script src="<?php echo dashboard_assets; ?>js/admin.js"></script>

    <!-- Custom tab JavaScript -->
    <script src="<?php echo dashboard_assets; ?>js/cbpFWTabs.js"></script>

    <script src="<?php echo site_url(); ?>theme/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script type="text/javascript">

    (function() {
        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
            new CBPFWTabs(el);
        });
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
    })();
    </script>

    </body>
</html>
