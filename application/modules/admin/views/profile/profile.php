<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Profile</h4> 
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url(); ?>">Home</a></li>
                <li><a href="<?php echo user_url; ?>dashboard">Dashboard</a></li>
                <li class="active">Profile</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- Blog-component -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-3 col-xs-12">
            <div class="white-box">
                <div class="user-bg">
                    <div class="overlay-box">
                        <div class="user-content">
                            <img src="<?php echo site_url(); ?>images/profile/user.png" class="thumb-lg img-circle profileImage" alt="img">
                            <h4 class="text-white"><?php echo $user['name']; ?></h4>
                            <h5 class="text-white"><?php echo $user['email']; ?></h5>
                            <!-- <h5 class="text-white">Last Logged : <?php echo $this->session->userdata('adminLastLogged'); ?></h5> -->
                        </div>
                    </div>
                </div>
                <?php if($this->session->userdata('adminLastLogged') != "") { ?>
                <div class="user-btm-box">
                    <div class="col-md-12 col-sm-12 text-center">
                        <h4>Last Logged On</h4> 
                    </div>
                    <div class="col-md-124 col-sm-12 text-center">
                        <h5><?php echo $this->session->userdata('adminLastLogged'); ?></h5>
                    </div>
                    <div class="col-md-124 col-sm-12 text-center">
                        <h5>@<?php echo $this->session->userdata('ipaddress'); ?></h5>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="white-box">
                <ul class="nav nav-tabs tabs customtab">
                    <li class="active tab">
                        <a href="#settings" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Account Settings</span> </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="settings">
                        <div class="row">
                            <div class="clearfix"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Name</label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <span class="data-value editableName nameView"><?php echo $user['name']; ?></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <a class="waves-effect waves-light formSidebarWithoutReset" data-value="changeName">
                                        <i class="ti-pencil"></i> Update
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Mobile</label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <span class="data-value editableMobile mobileView"><?php echo $user['mobile']; ?></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <a class="waves-effect waves-light formSidebarWithoutReset" data-value="changeName">
                                        <i class="ti-pencil"></i> Update
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Email</label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <span class="data-value editableEmail emailView"><?php echo $user['email']; ?></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <a class="waves-effect waves-light formSidebarWithoutReset" data-value="changeEmail" data-function="emailUpdateFormReset">
                                        <i class="ti-pencil"></i> Update
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Password</label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <span class="data-value editablePassword passwordView">********</span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <a class="waves-effect waves-light formSidebar editPassword" data-value="changePassword">
                                        <i class="ti-pencil"></i> Update
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

        <input type="hidden" id="currentPath" value="<?php echo site_url(); ?>admin/profile/">