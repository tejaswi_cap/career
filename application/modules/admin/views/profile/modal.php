<!-- Change Mobile Sidebar -->
<div class="right-sidebar" id="changeMobileSideBar">
    <div class="slimscrollright">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12 pull-right">
                <div class="rpanel-title">
                    <h4> Change Mobile <span class="pull-right"><i class="mdi mdi-arrow-right backArrow formSidebar" data-value="changeMobile"></i></span></h4>
                </div>
                <form class="form-material" role="form" id="changeMobileForm" enctype="multipart/form-data">
                    <div class="slimscrollright-body">
                        <div class="r-panel-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group required">
                                        <label>Mobile</label>
                                        <input type="text" name="mobile" id="mobile" placeholder="Enter Mobile Number" class="form-control changeMobile" data-error="Mobile Error" required="required" value="<?php echo $user['mobile']; ?>">
                                        <div class="help-block with-errors"></div>
                                        <div id="mobile_response" ></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <button type="button" id="resendBtn" class="btn btn-info m-t-10 btn-rounded mobileResend"><?php echo $this->lang->line('SENDOTP'); ?></button>
                                        <input type="hidden" name="resendOtpCount" id="resendOtpCount" value="0">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-8">
                                    <div id="otpDiv" class="form-group required" style="display: none;">
                                        <label><?php echo $this->lang->line('OTP'); ?></label>
                                        <input type="text" name="otp" id="otp" placeholder="<?php echo $this->lang->line('OTP_PLACEHOLDER'); ?>" class="form-control" data-error="<?php echo $this->lang->line('OTP_ERROR'); ?>" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rpanel-footer">
                            <button type="submit" class="btn btn-danger waves-effect waves-light save-category"><?php echo $this->lang->line('SAVE'); ?></button>
                            <button type="button" class="btn btn-white waves-effect formSidebar pull-left" data-value="changeMobile"><?php echo $this->lang->line('CLOSE'); ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Change Email Sidebar -->
<div class="right-sidebar" id="changeEmailSideBar">
    <div class="slimscrollright">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12 pull-right">
                <div class="rpanel-title">
                    <h4> Change Email <span class="pull-right"><i class="mdi mdi-arrow-right backArrow formSidebar" data-value="changeEmail"></i></span></h4>
                </div>
                <form class="" role="form" id="changeEmailForm" enctype="multipart/form-data">
                    <div class="slimscrollright-body">
                        <div class="r-panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group required">
                                        <label>Email</label>
                                        <input type="email" name="email" id="email" placeholder="Enter Email" class="form-control" data-error="Email required" required="required" value="<?php echo $user['email']; ?>">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 emailOTPDiv dn">
                                    <div class="form-group required">
                                        <label>OTP</label>
                                        <input type="text" name="otp" id="otp" placeholder="Enter OTP" class="form-control" data-error="OTP required" required="required">
                                        <div class="help-block with-errors"></div>
                                        <a class="pull-right sendEmailOTP">Resend OTP</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="rpanel-footer">
                            <button type="button" class="btn btn-danger sendEmailOTP sendEmailButton">Send OTP</button>
                            <button type="submit" class="btn btn-danger dn emailOTPDiv">SAVE</button>
                            <button type="button" class="btn btn-white waves-effect formSidebar pull-left" data-value="changeEmail">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<!-- Change Name Sidebar -->
<div class="right-sidebar" id="changeNameSideBar">
    <div class="slimscrollright">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12 pull-right">
                <div class="rpanel-title">
                    <h4> Change Name <span class="pull-right"><i class="mdi mdi-arrow-right backArrow formSidebar" data-value="changeName"></i></span></h4>
                </div>
                <form class="" role="form" id="changeNameForm" enctype="multipart/form-data">
                    <div class="slimscrollright-body">
                        <div class="r-panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group required">
                                        <label>Name</label>
                                        <input type="text" name="name" id="name" placeholder="Enter Name" class="form-control" data-error="Name required" required="required" value="<?php echo $user['name']; ?>">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group required">
                                        <label>Mobile</label>
                                        <input type="mobile" data-minlength="10" maxlength="10" name="mobile" id="mobile" placeholder="Enter Mobile Number" class="form-control" data-minlength-error="Mobile number must be minimum 10 digits" data-error="Mobile number required" required="required" value="<?php echo $user['mobile']; ?>">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rpanel-footer">
                            <button type="submit" class="btn btn-danger waves-effect waves-light save-category">Save</button>
                            <button type="button" class="btn btn-white waves-effect formSidebarWithoutReset pull-left" data-value="changeName">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Change Password Sidebar -->
<div class="right-sidebar" id="changePasswordSideBar">
    <div class="slimscrollright">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12 pull-right">
                <div class="rpanel-title">
                    <h4> Change Password <span class="pull-right"><i class="mdi mdi-arrow-right backArrow formSidebar" data-value="changePassword"></i></span></h4>
                </div>
                <form class="" role="form" id="changePasswordForm" enctype="multipart/form-data">
                    <div class="slimscrollright-body">
                        <div class="r-panel-body">
                            <div class="row"><div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="form-group required">
                                        <label>Current Pasword</label>
                                        <input type="password" name="oldpassword" id="oldpassword" placeholder="Enter Current Password" class="form-control" data-error="Current Password required" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="form-group required">
                                        <label>New Password</label>
                                        <input type="password" name="newpassword" id="newpassword" placeholder="Enter New Password" class="form-control" data-error="New Password required" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="form-group required">
                                        <label>Confirm Password</label>
                                        <input type="password" name="confirmpassword" id="confirmpassword" placeholder="Enter New Password" data-match="#newpassword"  class="form-control" data-match-error="Passwords are not matching" data-error="Confirm Password required" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rpanel-footer">
                            <button type="submit" class="btn btn-danger waves-effect waves-light save-category">Save</button>
                            <button type="button" class="btn btn-white waves-effect formSidebar pull-left" data-value="changePassword">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>