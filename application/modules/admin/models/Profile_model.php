<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Profile_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    
    public function getuserDetails() {

        $userId = $this->session->userdata('adminId');
        $query = "SELECT * FROM admin WHERE id = '".$userId."'";
        $result = $this->db->query($query);
        $result = $result->row_array();
        return $result;

    }

    public function sendOtpEmail() {

        $email = $this->session->userdata('adminEmail');
        $otp = rand(100000, 999999);
        $data = array(
            "email" => $email,
            "otp" => $otp,
            "createddate" => date("Y-m-d H:i:s")
        );
        $result = $this->db->insert("admin_otp", $data);
        if($result) {
            $response = array(
                "status" => "Success",
                "otp" => $otp,
                "message" => "OTP Sent Successfully"
            );
        } else {
            $response = array(
                "status" => "Failure",
                "message" => "Something went wrong please try again later"
            );
        }
        return $response;

    }
  
  
    public function editPassword($id) {

        $response = array();
        $newpassword=$this->input->post('newpassword');
        $data = array(
            "password" => md5($newpassword)
        );
        $this->db->where("id", $id);
        $result = $this->db->update("admin", $data);
        if($result) {
            $response = array(
                "response" => "Success", 
                "message" => "Password Updated Successfully"
            );
        } else {
            $response = array(
                "response" => "Failure", 
                "message" => "Something Went Wrong"
            );
        }
        return $response;
    }

    
    public function editProfile() {

        $response = array();
        extract($this->input->post());
        $id = $this->session->userdata('adminId');
        $query = "SELECT * FROM admin WHERE mobile = '".$mobile."' AND id != '".$id."'";
        $result = $this->db->query($query);
        $count = $result->num_rows();
        if($count == 0) {
            $data = array(
                "name" => $name,
                "mobile" => $mobile,
            );
            $this->db->where("id", $id);
            $result = $this->db->update("admin", $data);
            if($result) {
                $response = array(
                    "response" => "Success", 
                    "message" => "Profile Updated Successfully", 
                    "name" => $name,
                    "mobile" => $mobile,
                );
                $this->session->set_userdata('adminName',$name);
                $this->session->set_userdata('adminMobile',$mobile);
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Something Went Wrong"
                );
            }
        } else {
            $response = array(
                "response" => "Failure", 
                "message" => "Mobile Number Already Existed"
            );
        }
        
        return $response;
    }

    public function editEmail() {

        $response = array();
        extract($this->input->post());
        $id = $this->session->userdata('adminId');
        $existingEmail = $this->session->userdata('adminEmail');

        $query = "SELECT * FROM admin_otp WHERE email = '".$existingEmail."' ORDER BY id DESC LIMIT 0,1";
        $result = $this->db->query($query);
        $otpResult = $result->row_array();
        if($otpResult && $otpResult['otp'] == $otp) {
            $query = "SELECT * FROM admin WHERE email = '".$email."' AND id !='".$id."'";
            $result = $this->db->query($query);
            $emailCount= $result->num_rows();
            if($emailCount == 0){
                $data = array(
                    "email" => $email
                );
                $this->db->where("id", $id);
                $response = $this->db->update("admin", $data);
                $response = array(
                    "response" => "Success", 
                    "message" => "Email Id Updated Successfully", 
                    "email" => $email
                );
                $this->session->set_userdata('adminEmail',$email);
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Email Id Already Existed With Other User"
                );
            }
        } else {
            $response = array(
                "response" => "Failure", 
                "message" => "Enter Correct OTP"
            );
        }

        
        return $response;
    }


}