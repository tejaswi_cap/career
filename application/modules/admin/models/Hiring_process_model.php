<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Hiring_process_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    
    function getAllHiringProcess()
    {
        $query = "SELECT * FROM hiring_process WHERE is_active = 1 AND is_deleted = 0";
        $query = $this->db->query($query);
        return $query->result();
    }

}