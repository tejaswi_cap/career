<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Admin_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }


    public function getUserLogin() {

        $response = array();
        extract($this->input->post());
        $password = md5($password);
        $query = "SELECT * FROM admin WHERE email = '".$email."' AND password = '".$password."'";
        $result = $this->db->query($query);
        $count = $result->num_rows();
        if($count == 0) {
            $response = array(
                "status" => "Failure",
                "message" => "Invalid credentials"
            );
        } else {
            $result = $result->row_array();
            if($result['status'] == 1) {

                $loggedQuery = "SELECT * FROM admin_login WHERE user_id = '".$result['id']."' ORDER BY id DESC LIMIT 1, 2";
                $loggedResult = $this->db->query($loggedQuery);
                $loggedResult = $loggedResult->row_array();
                if($loggedResult) {
                    $userLastLogged = date("dS M, Y h:i A", strtotime($loggedResult['createddate']));
                    $ipaddress = $loggedResult['ipaddress'];
                } else {
                    $userLastLogged = "";
                    $ipaddress = "";
                }

                $newdata = array(
                    'adminId'  => $result['id'],
                    'adminName'  => $result['name'],
                    'adminEmail'  => $result['email'],
                    'adminMobile'  => $result['mobile'],
                    'adminImage'  => $result['image'],
                    'adminRole'  => 'ADMIN',
                    'adminDate'  => $result['createddate'],
                    'adminLastLogged'  => $userLastLogged,
                    'ipaddress'  => $ipaddress
                );
                $this->session->set_userdata($newdata);
                $data = array(
                    'user_id' => $result['id'],
                    "ipaddress" => $this->input->ip_address(),
                    "createddate" => date("Y-m-d H:i:s")
                );
                $this->db->insert('admin_login', $data);
                $response = array(
                    "status" => "Success",
                    "message" => "Successfully logged."
                );
            } else {
                $response = array(
                    "status" => "Failure",
                    "message" => "Your account has been deactivated with us. please contact admin to activate your account."
                );
            }
        }
        return $response;

    }


    public function resetPassword() {

        $response = array();
        extract($this->input->post());
        $query = "SELECT * FROM admin WHERE email = '".$email."'";
        $result = $this->db->query($query);
        $count = $result->num_rows();
        if($count == 0) {
            $response = array(
                "status" => "Failure",
                "message" => "Your email didn't registered with us."
            );
        } else {
            $result = $result->row_array();
            if($result['status'] == 1) {
                $password = rand(100000,999999);
                $data = array(
                    "password" => md5($password)
                );
                $this->db->where("id", $result['id']);
                $this->db->update("admin", $data);
                $response = array(
                    "status" => "Success",
                    "message" => "Your updated password has been sent to your email.",
                    "password" => $password
                );
            } else {
                $response = array(
                    "status" => "Failure",
                    "message" => "Your account has been deactivated with us. please contact admin to activate your account."
                );
            }
            
        }
        return $response;

    }



}
