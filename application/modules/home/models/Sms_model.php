<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Sms_model extends CI_Model
{
    public $authKey = "";
    public $senderId = "";
    public $route = "4";
    public $url="";
    public $otpUrl="";
    public $capNumber = "";
    public function __construct()
    {
        parent::__construct();
    }
    

    public function getmessage($id)
    {
        $query = "SELECT * FROM tbl_sms_templates WHERE id = '".$id."'";
        $result = $this->db->query($query);
        $result = $result->row_array();
        $message = $result['content'];
        return $message;
    }


    public function createOtp()
    {
        $otp = substr(number_format(time() * rand(), 0, '', ''), 0, 6);
        return $otp;
    }

    public function sendotp($mobileNo, $message, $otp)
    {
        $postData = array(
            'authkey' => $this->authKey,
            'mobiles' => '91'.$mobileNo,
            'message' => $message,
            'sender' => $this->senderId,
            'route' => $this->route,
            'country'=>'0',
            'otp' => $otp
        );
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $this->otpUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
    }

    public function send($mobileNo, $message)
    {
        $postData = array(
            'authkey' => $this->authKey,
            'mobiles' => '91'.$mobileNo,
            'message' => $message,
            'sender' => $this->senderId,
            'route' => $this->route,
            'country'=>'0'
        );
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
    }

}
