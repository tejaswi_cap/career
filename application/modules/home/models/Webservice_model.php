<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Webservice_model extends CI_Model
{

    // public $domain = "http://localhost:8000/";
    public $domain = "";
    public $token = "";

    public function __construct()
    {
        parent::__construct();
    }

    public function exampleFormData($apiData=array()) {

        $token = $this->session->userdata('userToken');
        $url = $this->domain."/";
        $document = new CURLFile(
            $_FILES['document']['tmp_name'],
            $_FILES['document']['type'],
            $_FILES['document']['name']
        );
        $apiData = array(
            "document" => $document
        );
        $response = $this->postDataWithFormData($token, $apiData, $url);
        return $response;
    }


    public function postData($data, $url)
    {
     
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        if($responseCode == 401) {
        	$this->logout();
        }
        $response = array(
        	"code" => $responseCode,
        	"result" => $output
        );
        return $response;
    }

    public function postDataWithFormData($token, $data, $url)
    {
     
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_BINARYTRANSFER => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(                                                                          
                'Content-Type: multipart/form-data',                                                                                
                'GIGATOKEN: '. $token                                                            
            )
        ));
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('GIGATOKEN:'.$token));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        if($responseCode == 401) {
            $this->logout();
        }
        $response = array(
            "code" => $responseCode,
            "result" => $output
        );
        return $response;
    }

    public function postDataWithHeaders($token, $data, $url)
    {
     
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data
        ));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('GIGATOKEN:'.$token));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        if($responseCode == 401) {
        	$this->logout();
        }
        $response = array(
        	"code" => $responseCode,
        	"result" => $output
        );
        return $response;
    }

    public function postDataWithJsonData($token, $data, $url)
    {
     
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_BINARYTRANSFER => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(                                                                          
                'Content-Type: application/json',                                                                                
                'GIGATOKEN: '. $token                                                            
            )
        ));
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('GIGATOKEN:'.$token));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        if($responseCode == 401) {
            $this->logout();
        }
        $response = array(
            "code" => $responseCode,
            "result" => $output
        );
        return $response;
    }


    public function putDataWithJsonData($token, $data, $url)
    {
     
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(                                                 
                'Content-Type: application/json',                        
                'GIGATOKEN: '. $token                                            
            )
        ));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        if($responseCode == 401) {
            $this->logout();
        }
        $response = array(
            "code" => $responseCode,
            "result" => $output
        );
        return $response;
    }


    public function putDataWithHeaders($token, $data, $url)
    {
     

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => $data
        ));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('GIGATOKEN:'.$token));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        if($responseCode == 401) {
            $this->logout();
        }
        $response = array(
            "code" => $responseCode,
            "result" => $output
        );
        return $response;
    }


    public function deleteDataWithHeaders($token, $url)
    {
     

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true
        ));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('GIGATOKEN:'.$token));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        if($responseCode == 401) {
            $this->logout();
        }
        $response = array(
            "code" => $responseCode,
            "result" => $output
        );
        return $response;
    }

    


    public function getDataWithHeaders($token, $url, $params=array())
    {
     
        $ch = curl_init();
        if(count($params) > 0)
        {
            $paramsKey = "?";
            foreach ($params as $key => $value) {
                $paramsKey .= $key.'='.$value;
            }
            $url .= $paramsKey;
        }
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true
        ));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('GIGATOKEN:'.$token));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        if($responseCode == 401) {
            $this->logout();
        }
        $response = array(
            "code" => $responseCode,
            "result" => $output
        );
        return $response;
    }


	public function logout() {

        $user_data = $this->session->all_userdata();
        $this->session->unset_userdata("userAccount");
        $this->session->unset_userdata("userId");
        $this->session->unset_userdata("userName");
        $this->session->unset_userdata("userEmail");
        $this->session->unset_userdata("userMobile");
        $this->session->unset_userdata("userDate");
        $this->session->unset_userdata("userClinic");
        $this->session->unset_userdata("userImage");
        redirect(site_url(), 'refresh');
    }
    
}
