<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
* --------------------------------------------------------------------
* Datatables Serverside script
* --------------------------------------------------------------------
*
*/  
class Datatable_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }



    public function getFilteredQuery($searchableFields, $sortableFields, $order=array(), $groupBy = "", $havingQuery = "") {
        
        $addedQuery = "";
        $i = 0;
        foreach ($searchableFields as $item)  
        {
            if(isset($_POST['search']['value']) && ($_POST['search']['value'] != "")) 
            {
                if($i===0) 
                {
                    $addedQuery .= " AND (  ".$item." LIKE '%".$_POST['search']['value']."%'";
                }
                else
                {
                    $addedQuery .= " OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
                }

                if(count($searchableFields) - 1 == $i) 
                    $addedQuery .= " )";
            }
            $i++;
        }
        if(isset($havingQuery) && $havingQuery != "")
        {
            $addedQuery .= " ". $havingQuery;
        }
        if(isset($groupBy) && $groupBy != "")
        {
            $addedQuery .= " GROUP BY ". $groupBy;
        }
        if(isset($_POST['order']) && in_array($_POST['order']['0']['column'], array_keys($sortableFields))) 
        {
            $addedQuery .= " ORDER BY ".$sortableFields[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir']." ";
        } 
        else if(isset($order) && count($order) > 0)
        {
            $addedQuery .= " ORDER BY ".key($order)." ".$order[key($order)]." ";
        }
        return $addedQuery;
    }

    public function getTableData($id) {

        $query = "SELECT id, name, html FROM datatable_dashboard WHERE id = ".$id;
        $result = $this->db->query($query);
        $result = $result->row_array();
        $message = $result['html'];
        return $message;
    }

    public function getSingleRowDetails($table, $id) {

        $query = "SELECT * FROM `".$table."` WHERE id = ".$id;
        $result = $this->db->query($query);
        $result = $result->row_array();
        return $result;
    }

    public function getSingleRowIsDelete($table, $id) {

        $query = "UPDATE `".$table."` SET is_deleted = '1' WHERE id = ".$id;
        $result = $this->db->query($query);
        return $result;
        
    }

    public function getRowDelete($table, $column, $id) {

        $this->db->where($column, $id);
        $result = $this->db->delete($table);
        return $result;
        
    }

    public function getSingleRowStatus($table, $id, $status) {

        $query = "UPDATE `".$table."` SET status = '".$status."' WHERE id = ".$id;
        $result = $this->db->query($query);
        return $result;
        
    }

    public function getactiveDetails($table, $column, $SQE)
    {
        $result = array();
        $query = "SELECT * FROM `".$table."`  WHERE status = '1' ORDER BY `".$column."` ".$SQE;
        $result = $this->db->query($query);
        $result = $result->result_array();
        return $result;
    }

    public function getsingleDetails($id, $table, $column)
    {
        $result = array();
        $query = "SELECT * FROM `".$table."` WHERE `".$column."` = '".$id."'";
        $result = $this->db->query($query);
        $result = $result->row_array();
        return $result;
    }

    public function getallDetails($table, $column, $SQE)
    {
        $result = array();
        $query = "SELECT * FROM `".$table."` WHERE is_deleted = '0' ORDER BY `".$column."` ".$SQE;
        
        $result = $this->db->query($query);
        $result = $result->result_array();
        return $result;
    }

}