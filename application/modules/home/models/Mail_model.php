<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mail_model extends CI_Model
{
    public $authKey = "124982AEt72rW7eCbN58e753b8";
    public $senderId = "GIGAdx";
    public $route = "4";
    public $url="https://control.msg91.com/api/sendhttp.php";

    public function __construct()
    {
        parent::__construct();
            $config = array(
            'SMTPAuth' => true,
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.mailtrap.io',
            'smtp_port' => 2525,
            'smtp_user' => '03ee6a745e0a9e',
            'smtp_pass' => '08ea32d56e5661',
            'mailtype' => 'html',
            'starttls'  => true,
            'charset' => 'iso-8859-1',
            'wordwrap' => true,
            'newline'   => "\r\n"
        );

        $this->load->library('email', $config);
    }


    public function getMailHeader()
    {
        $message = $this->getContent(1);
        $message    =   str_replace("{SENDEDDATE}", date("M d, Y"), $message['message']);
        return $message;
    }
    

    public function getMailFooter()
    {
        $message = $this->getContent(2);
        return $message['message'];
    }

    public function getContent($id)
    {
        $query = "SELECT * FROM tbl_mail_templates WHERE id = '".$id."'";
        $result = $this->db->query($query);
        $result = $result->row_array();
        $message = $result['content'];
        $message  = str_replace("{SITEURL}", site_url(), $message);
        $response = array("subject" => $result['subject'] , "message" => $message);
        return $response;
    }

    

    public function send($email, $message)
    {
        $subject = $message['subject'];
        $content = $message['message'];
        $this->email->from('noreply@cap.com', 'CAP');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($content);
        if (! $this->email->send()) {
            // $errors = $this->email->print_debugger();
            // echo $errors;
            return true;
        } else {
            return true;
        }
    }

    public function sendattachment($to_email, $message, $subject, $sender_name, $attachment=null, $bcc='')
    {
        $this->email->from('noreply@cap.com', $sender_name);
        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($message);
                
        if ($bcc != '') {
            $this->email->bcc($bcc);
        }
                
        if ($attachment!='') {
            $this->email->attach($attachment);
        }
                
        if ($this->email->send()) {
            return "Email sent ";
        } else {
            return "Failed";
        }
    }
    
}
