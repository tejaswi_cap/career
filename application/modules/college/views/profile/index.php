<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Profile</h4> 
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url(); ?>">Home</a></li>
                <li><a href="<?php echo user_url; ?>dashboard">Dashboard</a></li>
                <li class="active">Profile</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- Blog-component -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-3 col-xs-12">
            <div class="white-box">
                <div class="user-bg">
                    <div class="overlay-box">
                        <div class="user-content">
                            <img src="<?php echo $college['logo']; ?>" class="thumb-lg img-circle profileImage" alt="<?php echo $college['name']; ?>">
                            <h4 class="text-white"><?php echo $college['name']; ?></h4>
                            <h5 class="text-white"><?php echo $college['location']; ?></h5>
                            <!-- <h5 class="text-white">Last Logged : <?php echo $this->session->userdata('adminLastLogged'); ?></h5> -->
                        </div>
                    </div>
                </div>
                <?php if($this->session->userdata('adminLastLogged') != "") { ?>
                <div class="user-btm-box">

                </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="white-box">
                <ul class="nav nav-tabs tabs customtab">
                    <li class="active tab">
                        <a href="#collegeInfo" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">College Info</span> </a>
                    </li>
                    <li class="tab">
                        <a href="#collegeAddInfo" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Additional Info</span> </a>
                    </li>
                    <li class="tab">
                        <a href="#facility" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Facility</span> </a>
                    </li>
                    <li class="tab">
                        <a href="#brochure" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Brochure</span> </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="collegeInfo">
                        <form class="" role="form" id="profileForm" enctype="multipart/form-data">
                            <div class="form-body">
                                <input type="hidden" id="id" name="id" value="<?php echo $college['id']; ?>">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label>Name</label>
                                            <input type="text" id="name" name="name" class="form-control" required="required" data-error="College Name required" value="<?php echo $college['name']; ?>"> 
                                            <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label>Location</label>

                                            <select class="form-control" name="location_id" id="type" required="required" data-error="Location required">
                                                <option value="">Choose</option>
                                                <?php foreach($locations as $location) {
                                                    echo '<option value="'.$location->id.'"'. ($college['location_id'] == $location->id ? ' selected = "selected"':'').'>'.$location->name.'</option>';
                                                }?>
                                            </select> <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Sub location</label>
                                            <input type="text" id="sub_location" name="sub_location" class="form-control" value="<?php echo $college['sub_location']; ?>"> 
                                            <span class="help-block with-errors"></span>  </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Admissions</label>
                                            <div class="radio-list">
                                                <label class="radio-inline p-0">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="admissions" id="open" value="open" <?php if($college['admissions'] == "open"){ echo 'checked';} ?>>
                                                        <label for="open">Open</label>
                                                    </div>
                                                </label>
                                                <label class="radio-inline">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="admissions" id="close" value="close" <?php if($college['admissions'] == "close"){ echo 'checked';} ?>>
                                                        <label for="close">Close </label>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Website</label>
                                            <input type="text" id="website" name="website" class="form-control" value="<?php echo $college['website']; ?>"> 
                                            <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Year of establishment</label>
                                            <input type="text" id="year_of_establishment" name="year_of_establishment" class="form-control" value="<?php echo $college['year_of_establishment']; ?>"> 
                                            <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label>Logo</label>
                                        <input type="file" id="logo input-file-max-fs" name="logo" class="dropify" data-default-file="<?php echo $college['logo']; ?>" data-max-file-size="2M" />
                                        </div>  
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success save-category"> <i class="fa fa-check"></i> Save</button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="collegeAddInfo">
                        <form class="" role="form" id="profileAddForm" enctype="multipart/form-data">
                            <div class="form-body">
                                <input type="hidden" id="id" name="id" value="<?php echo $college['id']; ?>">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select class="form-control" name="type" id="type" >
                                                <option value="">Choose</option>
                                                <option value="1" <?php if($college['type'] == "1"){ echo 'selected';} ?>>Autonomous</option>
                                            </select>
                                            <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Approved By</label>

                                            <select class="form-control" name="approved_by" id="approved_by" >
                                                <option value="">Choose</option>
                                                <option value="1" <?php if($college['approved_by'] == "1"){ echo 'selected';} ?>>All India Council for Technical Education(AICTE)</option>
                                            </select> 
                                            <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Affiliated To</label>
                                            <select class="form-control" name="affiliated_to" id="affiliated_to">
                                                <option value="">Choose</option>
                                                <option value="1" <?php if($college['affiliated_to'] == "1"){ echo 'selected';} ?>>Jawaharlal Nehru Technological University</option>
                                            </select>
                                            <span class="help-block with-errors"></span>  
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Admission Criteria</label>
                                            <input type="text" id="admission_criteria" name="admission_criteria" class="form-control"  value="<?php echo $college['admission_criteria']; ?>"> 
                                            <span class="help-block with-errors"></span>  
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success save-category"> <i class="fa fa-check"></i> Save</button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="facility">
                        <form class="" role="form" id="facilityForm" enctype="multipart/form-data">
                            <div class="form-body">
                                <input type="hidden" id="id" name="id" value="<?php echo $college['id']; ?>">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Library</label>
                                            <div class="radio-list">
                                                <label class="radio-inline p-0">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="library" id="library1" value="yes" <?php if($college['library'] == "yes"){ echo 'checked';} ?>>
                                                        <label for="library1">Yes</label>
                                                    </div>
                                                </label>
                                                <label class="radio-inline">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="library" id="library2" value="no" <?php if($college['library'] == "no"){ echo 'checked';} ?>>
                                                        <label for="library2">No </label>
                                                    </div>
                                                </label>
                                            </div>
                                        </div> 
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Sports</label>
                                            <div class="radio-list">
                                            <label class="radio-inline p-0">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="sports" id="sports1" value="yes" <?php if($college['sports'] == "yes"){ echo 'checked';} ?>>
                                                        <label for="sports1">Yes</label>
                                                    </div>
                                                </label>
                                                <label class="radio-inline">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="sports" id="sports2" value="no" <?php if($college['sports'] == "no"){ echo 'checked';} ?>>
                                                        <label for="sports2">No </label>
                                                    </div>
                                                </label>
                                            </div>
                                        </div> 
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Gym</label>
                                            <div class="radio-list">
                                            <label class="radio-inline p-0">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="gym" id="gym1" value="yes" <?php if($college['gym'] == "yes"){ echo 'checked';} ?>>
                                                        <label for="gym1">Yes</label>
                                                    </div>
                                                </label>
                                                <label class="radio-inline">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="gym" id="gym2" value="no" <?php if($college['gym'] == "no"){ echo 'checked';} ?>>
                                                        <label for="gym2">No </label>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>  
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">AC campus</label>
                                            <div class="radio-list">
                                            <label class="radio-inline p-0">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="ac_campus" id="ac_campus1" value="yes" <?php if($college['ac_campus'] == "yes"){ echo 'checked';} ?>>
                                                        <label for="ac_campus1">Yes</label>
                                                    </div>
                                                </label>
                                                <label class="radio-inline">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="ac_campus" id="ac_campus2" value="no" <?php if($college['ac_campus'] == "no"){ echo 'checked';} ?>>
                                                        <label for="ac_campus2">No </label>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Computer lab</label>
                                            <div class="radio-list">
                                            <label class="radio-inline p-0">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="computer_lab" id="computer_lab1" value="yes" <?php if($college['computer_lab'] == "yes"){ echo 'checked';} ?>>
                                                        <label for="computer_lab1">Yes</label>
                                                    </div>
                                                </label>
                                                <label class="radio-inline">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="computer_lab" id="computer_lab2" value="no" <?php if($college['computer_lab'] == "no"){ echo 'checked';} ?>>
                                                        <label for="computer_lab2">No </label>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success save-category"> <i class="fa fa-check"></i> Save</button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="brochure">
                        <form action="<?php echo site_url(); ?>college/profile/editbrochure" class="dropzone">
                            <div class="fallback">
                                <input name="brochure" id="brochure" type="file" multiple /> </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="currentPath" value="<?php echo site_url(); ?>college/profile/">
