<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">All courses</h4> 
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url(); ?>">Home</a></li>
                <li><a href="<?php echo user_url; ?>dashboard">Dashboard</a></li>
                <li class="active">All courses</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- Blog-component -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="box-title m-b-30">All courses</h3>
                    </div>
                    <div class="col-md-3 ">
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="myTable" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Qualification</th>
                                <th>Name</th>
                                <th>Duration</th>
                                <th>Total Fee</th>
                                <th>Acedamic Eligibility</th>
                                <th>Entrance Test</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($courses as $course){?>
                            <tr>
                                <td ><?php echo $course->qualification ?></td>
                                <td ><?php echo $course->course_name ?></td>
                                <td ><?php echo $course->duration_years ?> years, <?php echo $course->duration_months ?> months</td>
                                <td ><?php echo $course->total_fee ?></td>
                                <td ><?php echo $course->academic_eligibility ?></td>
                                <td ><?php echo $course->entrance_test ?></td>
                                <td>
                                    <a href="<?php echo site_url('college/course/updatecourse/'.$course->id); ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-md m-r-5"><i class="ti-pencil-alt"></i></button></a>
                                    <button type="button" class="btn btn-info btn-outline btn-circle btn-md m-r-5 deleteCourse" data-id="<?php echo $course->id ?>"><i class="icon-trash"></i></button>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
            <div class="panel">
                <div class="panel-body">                                
                    <a href="<?php echo site_url(); ?>college/course/addcourse" class="btn btn-info">Add course</a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
            <div class="panel wallet-widgets">
                <ul class="wallet-list">
                    <li><a href="<?php echo site_url(); ?>college/profile">Edit profile</a></li>
                    <li><a href="<?php echo site_url(); ?>college/faculty">Faculty</a></li>
                    <li><a href="<?php echo site_url(); ?>college/faculty/addfaculty">Add faculty</a></li>
                </ul>
            </div>
        </div>
    </div>

</div>
<input type="hidden" id="currentPath" value="<?php echo site_url(); ?>college/course/">
