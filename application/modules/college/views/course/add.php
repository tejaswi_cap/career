<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php if($course['id'] == '') echo 'Add Course'; else echo 'Edit Course';?></h4> 
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url(); ?>">Home</a></li>
                <li><a href="<?php echo user_url; ?>dashboard">Dashboard</a></li>
                <li class="active"><?php if($course['id'] == '') echo 'Add Course'; else echo 'Edit Course';?></li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- Blog-component -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <div class="panel panel-info">
                <div class="panel-heading"><?php if($course['id'] == '') echo 'Add Course'; else echo 'Edit Course';?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form class="" role="form" id="courseForm" enctype="multipart/form-data">
                            <div class="form-body">
                                <input type="hidden" id="id" name="id" value="<?php echo $course['id']; ?>">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label>Qualification</label>
                                            <select class="form-control" name="qualification_id" id="type" required="required" data-error="Qualification required">
                                                <option value="">Choose</option>
                                                <?php foreach($qualifications as $qualification) {
                                                    echo '<option value="'.$qualification->id.'"'. ($course['qualification_id'] == $qualification->id ? ' selected = "selected"':'').'>'.$qualification->name.'</option>';
                                                }?>
                                            </select> 
                                            <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label>Course</label>
                                            <?php 
                                                $qualification = array();
                                                foreach($course_branch as $branch)
                                                { 
                                                    $qualification[$branch->qualification][] = $branch;
                                                }
                                            ?>
                                            <select class="form-control" name="course_id" id="type" required="required" data-error="Branch required">
                                                <option value="">Choose</option>
                                                <?php foreach($qualification as $key => $branch) {?>
                                                    <optgroup label="<?php echo $key?>">
                                                        <?php foreach($branch as $branch) {
                                                            echo '<option value="'.$branch->id.'"'. ($course['course_id'] == $branch->id ? ' selected = "selected"':'').'>'.$branch->name.'</option>';
                                                        }?>
                                                    </optgroup>
                                                <?php }?>
                                                
                                            </select> 
                                            <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label>Yearly Fee</label>
                                            <input type="number"  name="yearly_fee" id="yearly_fee" class="form-control" required="required" data-error="Yearly fee required" value="<?php echo $course['yearly_fee']; ?>">
                                            <span class="help-block with-errors"></span>  </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Duration</label>
                                            <select class=" form-control" name="duration_years" id="duration_years">			
                                                <?php 
                                                    for($i = 0 ; $i < 10; $i++){
                                                        echo '<option value="'.$i.'"'. ($course['duration_years'] == $i ? ' selected = "selected"':'').'>'.$i.' years </option>';
                                                    }
                                                ?>
                                            </select>
                                            <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>.</label>
                                            <select class=" form-control" name="duration_months" id="duration_months">			
                                                <?php 
                                                    for($i = 0 ; $i < 12; $i++){
                                                        echo '<option value="'.$i.'"'. ($course['duration_months'] == $i ? ' selected = "selected"':'').'>'.$i.' months </option>';
                                                    }
                                                ?>
                                            </select>                                            
                                            <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label>Total Fee</label>
                                            <input type="number"  name="total_fee" id="total_fee" class="form-control" required="required" data-error="Total fee required" value="<?php echo $course['total_fee']; ?>">
                                            <span class="help-block with-errors"></span> </div>                                               
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label">Academic eligibility</label>
                                        <select class="form-control" name="academic_eligibility" id="academic_eligibility" required="required" data-error="Academic eligibility required">
                                            <option value="1" >10+2</option>
                                        </select> 
                                        <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Entrance Test</label>
                                            <select class="form-control" name="entrance_test" id="entrance_test" required="required" data-error="Entrance Test required">
                                                <option value="1" >Ts-Eamcet</option>
                                            </select> 
                                            <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success save-category"> <i class="fa fa-check"></i> Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
            <div class="panel wallet-widgets">
                <ul class="wallet-list">
                    <li><a href="<?php echo site_url(); ?>college/profile">Edit profile</a></li>
                    <li><a href="<?php echo site_url(); ?>college/course">All Courses</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="currentPath" value="<?php echo site_url(); ?>college/course/">
