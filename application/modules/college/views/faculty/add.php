<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php if($faculty['id'] == '') echo 'Add Faculty'; else echo 'Edit Faculty';?></h4> 
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url(); ?>">Home</a></li>
                <li><a href="<?php echo user_url; ?>dashboard">Dashboard</a></li>
                <li class="active"><?php if($faculty['id'] == '') echo 'Add Faculty'; else echo 'Edit Faculty';?></li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- Blog-component -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <div class="panel panel-info">
                <div class="panel-heading"><?php if($faculty['id'] == '') echo 'Add Faculty'; else echo 'Edit Faculty';?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form class="" role="form" id="facultyForm" enctype="multipart/form-data">
                            <div class="form-body">
                                <input type="hidden" id="id" name="id" value="<?php echo $faculty['id']; ?>">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label>Name</label>
                                            <input type="text"  name="name" id="name" class="form-control" required="required" data-error="Name required" value="<?php echo $faculty['name']; ?>">
                                            <span class="help-block with-errors"></span> </div>                                               
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label>Qualification</label>
                                            <select class="form-control" name="qualification_id" id="type" required="required" data-error="Qualification required">
                                                <option value="">Choose</option>
                                                <?php foreach($qualifications as $qualification) {
                                                    echo '<option value="'.$qualification->id.'"'. ($faculty['qualification_id'] == $qualification->id ? ' selected = "selected"':'').'>'.$qualification->name.'</option>';
                                                }?>
                                            </select> 
                                            <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label>Department</label>
                                            <?php 
                                                $qualification = array();
                                                foreach($course_branch as $branch)
                                                { 
                                                    $qualification[$branch->qualification][] = $branch;
                                                }
                                            ?>
                                            <select class="form-control" name="course_id" id="type" required="required" data-error="Branch required">
                                                <option value="">Choose</option>
                                                <?php foreach($qualification as $key => $branch) {?>
                                                    <optgroup label="<?php echo $key?>">
                                                        <?php foreach($branch as $branch) {
                                                            echo '<option value="'.$branch->id.'"'. ($faculty['course_id'] == $branch->id ? ' selected = "selected"':'').'>'.$branch->name.'</option>';
                                                        }?>
                                                    </optgroup>
                                                <?php }?>
                                                
                                            </select> 
                                            <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Designation</label>
                                            <select class="form-control" name="designation" id="designation" >
                                                <option value="">Choose</option>
                                                <option value="1" <?php if($faculty['designation'] == "1"){ echo 'selected';} ?>>Assistant Professor</option>
                                            </select>
                                           <span class="help-block with-errors"></span>  </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Experience</label>
                                            <input type="number"  name="experience" id="experience" class="form-control" value="<?php echo $faculty['experience']; ?>">
                                            <span class="help-block with-errors"></span>  </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label>Image</label>
                                        <input type="file" id=" input-file-max-fs" name="image" class="dropify" data-default-file="<?php echo $faculty['image']; ?>" data-max-file-size="2M" />
                                        </div>  
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Description</label>
                                            <textarea class="form-control" rows="15" cols="50" id="description" name="description" required="required" data-error="Description required"><?php echo $faculty['description']; ?></textarea>
                                            <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success save-category"> <i class="fa fa-check"></i> Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
            <div class="panel wallet-widgets">
                <ul class="wallet-list">
                    <li><a href="<?php echo site_url(); ?>college/profile">Edit profile</a></li>
                    <li><a href="<?php echo site_url(); ?>college/course">All Courses</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="currentPath" value="<?php echo site_url(); ?>college/faculty/">
