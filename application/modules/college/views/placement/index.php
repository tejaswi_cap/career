<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Placement</h4> 
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url(); ?>">Home</a></li>
                <li><a href="<?php echo user_url; ?>dashboard">Dashboard</a></li>
                <li class="active">Placement</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- Blog-component -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="box-title m-b-30">Placement</h3>
                    </div>
                    <div class="col-md-3 ">
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="myTable" class="table table-striped">
                        <thead>
                            <tr>
                                <th>No. of Tieup Companies</th>
                                <th>No of students selected</th>
                                <th>Batch year</th>
                                <th>Average package</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($placements as $placement){?>
                            <tr>
                                <td ><?php echo $placement->tieup_companies ?></td>
                                <td ><?php echo $placement->students_selected ?> </td>
                                <td ><?php echo $placement->batch_year ?> </td>
                                <td ><?php echo $placement->average_package ?></td>
                                <td>
                                    <a href="<?php echo site_url('college/placement/updateplacement/'.$placement->id); ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-md m-r-5"><i class="ti-pencil-alt"></i></button></a>
                                    <button type="button" class="btn btn-info btn-outline btn-circle btn-md m-r-5 deletePlacement" data-id="<?php echo $placement->id ?>"><i class="icon-trash"></i></button>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
            <div class="panel">
                <div class="panel-body">                                
                    <a href="<?php echo site_url(); ?>college/placement/addplacement" class="btn btn-info">Add placement</a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
            <div class="panel wallet-widgets">
                <ul class="wallet-list">
                    <li><a href="<?php echo site_url(); ?>college/profile">Edit profile</a></li>
                    <li><a href="<?php echo site_url(); ?>college/course">All courses</a></li>
                    <li><a href="<?php echo site_url(); ?>college/faculty">All faculty</a></li>
                </ul>
            </div>
        </div>
    </div>

</div>
<input type="hidden" id="currentPath" value="<?php echo site_url(); ?>college/placement/">
