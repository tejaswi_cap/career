<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php if($placement['id'] == '') echo 'Add Placement'; else echo 'Edit Placement';?></h4> 
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url(); ?>">Home</a></li>
                <li><a href="<?php echo user_url; ?>dashboard">Dashboard</a></li>
                <li class="active"><?php if($placement['id'] == '') echo 'Add Placement'; else echo 'Edit Placement';?></li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- Blog-component -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <div class="panel panel-info">
                <div class="panel-heading"><?php if($placement['id'] == '') echo 'Add Placement'; else echo 'Edit Placement';?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form class="" role="form" id="placementForm" enctype="multipart/form-data">
                            <div class="form-body">
                                <input type="hidden" id="id" name="id" value="<?php echo $placement['id']; ?>">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>No of tieup Companies</label>
                                            <input type="number"  name="tieup_companies" id="tieup_companies" class="form-control" value="<?php echo $placement['tieup_companies']; ?>">
                                            <span class="help-block with-errors"></span> </div>                                               
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>No of students selected</label>
                                            <input type="number"  name="students_selected" id="students_selected" class="form-control" value="<?php echo $placement['students_selected']; ?>"> 
                                            <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Batch year</label>
                                            <select class="form-control" name="batch_year" id="batch_year" >
                                                <option value="">Choose</option>
                                                <option value="2018-2019" <?php if($placement['batch_year'] == "2018-2019"){ echo 'selected';} ?>>2018-2019</option>
                                            </select>
                                            <span class="help-block with-errors"></span> </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Average package</label>
                                            <input type="number"  name="average_package" id="average_package" class="form-control" value="<?php echo $placement['average_package']; ?>"> 
                                           <span class="help-block with-errors"></span>  </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success save-category"> <i class="fa fa-check"></i> Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
            <div class="panel wallet-widgets">
                <ul class="wallet-list">
                    <li><a href="<?php echo site_url(); ?>college/profile">Edit profile</a></li>
                    <li><a href="<?php echo site_url(); ?>college/course">All Courses</a></li>
                    <li><a href="<?php echo site_url(); ?>college/faculty">All faculty</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="currentPath" value="<?php echo site_url(); ?>college/placement/">
