<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Placement_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getCollegePlacements($id) {

        $query = "SELECT * FROM college_placement WHERE college_id = '".$id."'";
        $result = $this->db->query($query);
        return $result->result();

    }

    public function getPlacementDetails($id) {

        $query = "SELECT * FROM college_placement WHERE id = '".$id."'";
        $result = $this->db->query($query);
        return $result->row_array();

    }

    public function editPlacement() {

        $response = array();
        extract($this->input->post());
        $college_id = $this->session->userdata('adminId');
        if($id == ''){
            $id = '';
        }
        $query = "SELECT * FROM college_placement WHERE id = '".$id."'";
        $result = $this->db->query($query);
        $count = $result->num_rows();
        $data = array(
            "college_id" => $college_id,
        );
        if(isset($tieup_companies)){
            $data['tieup_companies'] = $tieup_companies;
        }
        if(isset($students_selected)){
            $data['students_selected'] = $students_selected;
        }        
        if(isset($batch_year)){
            $data['batch_year'] = $batch_year;
        }        
        if(isset($average_package)){
            $data['average_package'] = $average_package;
        }

        if($count == 0 ) {
            $data['created_by'] = $college_id;
            $this->db->set($data);
            $result = $this->db->insert("college_placement");
            if($result) {
                $response = array(
                    "response" => "Success", 
                    "message" => "Faculty added Successfully",
                );
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Something Went Wrong",
                );
            }

            
        } else {
            $data['updated_by'] = $college_id;
            $this->db->where("id", $id);
            $result = $this->db->update("college_placement", $data);
                       
            if($result) {
                $response = array(
                    "response" => "Success", 
                    "message" => "Placement Updated Successfully", 
                );
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Something Went Wrong"
                );
            }
        }
        
        return $response;
    }

    public function deletePlacement($id) {

        $response = array();
        
            $this->db->where("id", $id);
            $result = $this->db->delete("college_placement");
                       
            if($result) {
                $response = array(
                    "response" => "Success", 
                    "message" => "Placement Deleted Successfully", 
                );
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Something Went Wrong"
                );
            }
        
        return $response;

    }


}