<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Course_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getCollegeCourses($id) {

        $query = "SELECT a.*,b.name as qualification, c.name as course_name FROM college_course a
                    JOIN qualification b ON b.id = a.qualification_id
                    JOIN course_branch c ON c.id = a.course_id
                    WHERE a.college_id = '".$id."'";
        $result = $this->db->query($query);
        return $result->result();

    }

    public function getCourseDetails($id) {

        $query = "SELECT a.*,b.name as qualification, c.name as course_name FROM college_course a
                    JOIN qualification b ON b.id = a.qualification_id
                    JOIN course_branch c ON c.id = a.course_id
                    WHERE a.id = '".$id."'";
        $result = $this->db->query($query);
        return $result->row_array();

    }

    public function editCourse() {

        $response = array();
        extract($this->input->post());
        $college_id = $this->session->userdata('adminId');
        if($id == ''){
            $id = '';
        }
        $query = "SELECT * FROM college_course WHERE id = '".$id."'";
        $result = $this->db->query($query);
        $count = $result->num_rows();
        $data = array(
            "college_id" => $college_id,
            "qualification_id" => $qualification_id,
            "course_id" => $course_id,
            "yearly_fee" => $yearly_fee,
            "duration_years" => $duration_years,
            "duration_months" => $duration_months,
            "total_fee" => $total_fee,
            "academic_eligibility" => $academic_eligibility,
            "entrance_test" => $entrance_test,
        );

        if($count == 0 ) {
            $data['created_by'] = $college_id;
            $this->db->set($data);
            $result = $this->db->insert("college_course");
            if($result) {
                $response = array(
                    "response" => "Success", 
                    "message" => "Course added Successfully",
                );
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Something Went Wrong",
                );
            }

            
        } else {
            $data['updated_by'] = $college_id;
            $this->db->where("id", $id);
            $result = $this->db->update("college_course", $data);
                       
            if($result) {
                $response = array(
                    "response" => "Success", 
                    "message" => "Course Updated Successfully", 
                );
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Something Went Wrong"
                );
            }
        }
        
        return $response;
    }

    public function deleteCourse($id) {

        $response = array();
        
            $this->db->where("id", $id);
            $result = $this->db->delete("college_course");
                       
            if($result) {
                $response = array(
                    "response" => "Success", 
                    "message" => "Course Deleted Successfully", 
                );
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Something Went Wrong"
                );
            }
        
        return $response;

    }


}