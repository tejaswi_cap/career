<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Faculty extends CI_Controller {
    
    //set Header page
    public $headerPage = '../../views/admin/header';
    
    //set footer page
    public $footerPage = '../../views/admin/footer';
    
    public function __Construct() {

        parent::__Construct();
        $this->load->model('Faculty_model', 'my_model');
        $this->load->model('admin/qualification_model','qualification_model');
        $this->load->model('admin/course_branch_model','course_branch_model');
        if ($this->session->userdata('adminId') == '') {
            redirect(base_url());
        }

    }

    public function index() {
        $id = $this->session->userdata('adminId');
        $data['faculty'] = $this->my_model->getCollegeFaculty($id);
        $data['qualifications'] = $this->qualification_model->getAllQualifications();
        $data['course_branch'] = $this->course_branch_model->getAllCourseBranches();
        $this->load->view($this->headerPage, $data);
        $this->load->view("faculty/index", $data);
        $this->load->view($this->footerPage, $data);

    }

    public function addfaculty() {

        $data['faculty'] = array(
    	    'id' => set_value('id'),
    	    'name' => set_value('name'),
    	    'qualification_id' => set_value('qualification_id',''),
    	    'experience' => set_value('experience'),
    	    'course_id' => set_value('course_id'),
    	    'designation' => set_value('designation'),
    	    'image' => set_value('image'),
    	    'description' => set_value('description')
	    );
        $data['qualifications'] = $this->qualification_model->getAllQualifications();
        $data['course_branch'] = $this->course_branch_model->getAllCourseBranches();
        $this->load->view($this->headerPage, $data);
        $this->load->view("faculty/add", $data);
        $this->load->view($this->footerPage, $data);


    }

    public function savefaculty() {

        $result = $this->my_model->editFaculty();
        echo json_encode($result);

    }

    public function updatefaculty($id) {
        $data['faculty'] = $this->my_model->getFacultyDetails($id);
        $data['qualifications'] = $this->qualification_model->getAllQualifications();
        $data['course_branch'] = $this->course_branch_model->getAllCourseBranches();
        $this->load->view($this->headerPage, $data);
        $this->load->view("faculty/add", $data);
        $this->load->view($this->footerPage, $data);

    }

    public function deletefaculty() {
        $id = $_POST['id'];
        $result = $this->my_model->deleteFaculty($id);
        echo json_encode($result);

    }

}
