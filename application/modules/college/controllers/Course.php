<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Course extends CI_Controller {
    
    //set Header page
    public $headerPage = '../../views/college/header';
    
    //set footer page
    public $footerPage = '../../views/college/footer';
    
    public function __Construct() {

        parent::__Construct();
        $this->load->model('Course_model', 'my_model');
        $this->load->model('admin/qualification_model','qualification_model');
        $this->load->model('admin/course_branch_model','course_branch_model');
        if ($this->session->userdata('adminId') == '') {
            redirect(base_url());
        }

    }

    public function index() {
        $id = $this->session->userdata('adminId');
        $data['courses'] = $this->my_model->getCollegeCourses($id);
        $data['qualifications'] = $this->qualification_model->getAllQualifications();
        $data['course_branch'] = $this->course_branch_model->getAllCourseBranches();
        $this->load->view($this->headerPage, $data);
        $this->load->view("course/index", $data);
        $this->load->view($this->footerPage, $data);

    }

    public function addcourse() {

        $data['course'] = array(
    	    'id' => set_value('id'),
    	    'qualification' => set_value('qualification',''),
    	    'course_id' => set_value('course_id'),
    	    'yearly_fee' => set_value('yearly_fee'),
    	    'duration_years' => set_value('duration_years'),
    	    'duration_months' => set_value('duration_months'),
    	    'total_fee' => set_value('total_fee'),
    	    'academic_eligibility' => set_value('academic_eligibility'),
    	    'entrance_test' => set_value('entrance_test'),

	    );
        $data['qualifications'] = $this->qualification_model->getAllQualifications();
        $data['course_branch'] = $this->course_branch_model->getAllCourseBranches();
        $this->load->view($this->headerPage, $data);
        $this->load->view("course/add", $data);
        $this->load->view($this->footerPage, $data);


    }

    public function savecourse() {

        $result = $this->my_model->editCourse();
        echo json_encode($result);

    }

    public function updatecourse($id) {
        $data['course'] = $this->my_model->getCourseDetails($id);
        $data['qualifications'] = $this->qualification_model->getAllQualifications();
        $data['course_branch'] = $this->course_branch_model->getAllCourseBranches();
        $this->load->view($this->headerPage, $data);
        $this->load->view("course/add", $data);
        $this->load->view($this->footerPage, $data);

    }

    public function deletecourse() {
        $id = $_POST['id'];
        $result = $this->my_model->deleteCourse($id);
        echo json_encode($result);

    }

}
