<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Placement extends CI_Controller {
    
    //set Header page
    public $headerPage = '../../views/admin/header';
    
    //set footer page
    public $footerPage = '../../views/admin/footer';
    
    public function __Construct() {

        parent::__Construct();
        $this->load->model('Placement_model', 'my_model');
        if ($this->session->userdata('adminId') == '') {
            redirect(base_url());
        }

    }

    public function index() {
        $id = $this->session->userdata('adminId');
        $data['placements'] = $this->my_model->getCollegePlacements($id);
        $this->load->view($this->headerPage, $data);
        $this->load->view("placement/index", $data);
        $this->load->view($this->footerPage, $data);

    }

    public function addplacement() {

        $data['placement'] = array(
    	    'id' => set_value('id'),
    	    'tieup_companies' => set_value('tieup_companies'),
    	    'students_selected' => set_value('students_selected',''),
    	    'batch_year' => set_value('batch_year'),
    	    'average_package' => set_value('average_package'),
	    );
        $this->load->view($this->headerPage, $data);
        $this->load->view("placement/add", $data);
        $this->load->view($this->footerPage, $data);


    }

    public function saveplacement() {

        $result = $this->my_model->editPlacement();
        echo json_encode($result);

    }

    public function updateplacement($id) {
        $data['placement'] = $this->my_model->getPlacementDetails($id);
        $this->load->view($this->headerPage, $data);
        $this->load->view("placement/add", $data);
        $this->load->view($this->footerPage, $data);

    }

    public function deleteplacement() {
        $id = $_POST['id'];
        $result = $this->my_model->deletePlacement($id);
        echo json_encode($result);

    }

}
