        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <div class="top-left-part">
                    <!-- Logo -->
                    <a class="logo" href="<?php echo site_url(); ?>">
                        <!-- Logo icon image, you can use font-icon also --><b>
                        <!--This is dark logo icon-->
                        <!-- <img src="<?php echo dashboard_assets; ?>images/favicon.png" alt="home" class="dark-logo" /> -->
                        <!--This is light logo icon-->
                        <!-- <img src="<?php echo dashboard_assets; ?>images/favicon.png" alt="home" class="light-logo" /> -->
                     </b>
                        <!-- Logo text image you can use text also --><span class="hidden-xs">
                        <!-- <img src="<?php echo dashboard_assets; ?>images/logo.png" alt="home" /> -->
                     </span> </a>
                </div>
                <!-- /Logo -->
                <?php if($this->session->userdata('adminLastLogged') != "") { ?>
                <ul class="nav navbar-top-links navbar-left">
                    <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs"><i class="ti-close ti-menu"></i></a></li>
                    <!-- .Megamenu -->
                    <li class="mega-dropdown"> 
                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#">
                            <span class="hidden-xs">Last Logged On : <?php echo $this->session->userdata('adminLastLogged'); ?></span> 
                        </a>
                    </li>
                    <!-- /.Megamenu -->
                </ul>
                <?php } ?>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="<?php echo site_url(); ?>images/profile/user.png" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"><?php echo $this->session->userdata('adminName'); ?></b><span class="caret"></span> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="<?php echo site_url(); ?>images/profile/user.png" alt="user" /></div>
                                    <div class="u-text">
                                        <h4><?php echo $this->session->userdata('adminName'); ?></h4>
                                        <p class="text-muted"><?php echo $this->session->userdata('adminEmail'); ?></p><a href="<?php echo site_url(); ?>user/profile" class="btn btn-rounded btn-danger btn-sm btnAccount" data-value="clinic">View Profile</a>
                                    </div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo site_url(); ?>admin/profile"><i class="ti-settings"></i> Account Settings</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo site_url(); ?>admin/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>