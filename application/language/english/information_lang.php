
<?php




//defined new variables for user dashboard

$lang['MESSAGE_ADD'] 			= 	'Added Successfully';
$lang['MESSAGE_UPDATE'] 		= 	'Updated Successfully';
$lang['MESSAGE_DELETE'] 		= 	'Deleted Successfully';
$lang['MESSAGE_STATUS'] 		= 	'Status Updated Successfully';
$lang['MESSAGE_500'] 			= 	'Something Went Wrong';
$lang['MESSAGE_EXIST'] 			= 	'Already Exists.';


$lang['SNO'] 					= 	'S.No';
$lang['ACTIVITY'] 				= 	'Activity';
$lang['DASHBOARD'] 				= 	'Dashboard';
$lang['HOME'] 					= 	'Home';
$lang['APPROVAL'] 				= 	'Approval';
$lang['STATUS'] 				= 	'Status';
$lang['APPROVED'] 				= 	'Approved';
$lang['PENDING'] 				= 	'Pending';
$lang['ACTIVE'] 				= 	'Active';
$lang['DEACTIVE'] 				= 	'Inactive';


$lang['BULKSMS_MESSAGE_ADD'] 			= 	'Raised Bulk SMS Request Successfully';


//***** lable class names *****//
$lang['LABLESUCCESS']		=	'label-success';
$lang['LABLEDANGER']		=	'label-danger';

//***** defined buttom content *****//

$lang['SAVE'] 				= 	'Save';
$lang['CLOSE'] 				= 	'Close';
$lang['CANCEL'] 			= 	'Cancel';
$lang['IMPORT'] 			= 	'Import';
$lang['EXPORT'] 			= 	'Export';


//***** defined lable content for user dashboard *****//

$lang['NAME'] 					= 	'Name';
$lang['EMAIL'] 					= 	'Email';
$lang['MOBILE'] 				= 	'Mobile';
$lang['AMOUNT'] 				= 	'Amount';
$lang['MANUFACTURER'] 			= 	'Manufacturer';
$lang['AVAILABLECOUNT'] 		= 	'Available Count';
$lang['MINIMUMCOUNT'] 			= 	'Minimum Count';
$lang['DISCOUNT'] 				= 	'Discount(INR)';
$lang['TAX'] 					= 	'Tax(%)';
$lang['DESCRIPTION'] 			= 	'Description';
$lang['CONTENT'] 				= 	'Content';
$lang['DOB'] 					= 	'Date of Birth';
$lang['AGE'] 					= 	'Age';
$lang['CREATEDDATE'] 			= 	'Created Date';
$lang['EXPENSESTYPE'] 			= 	'Expense Type';
$lang['SCHEDULED'] 				= 	'Scheduled Time';
$lang['BULKSMS'] 				= 	'Bulk SMS';
$lang['PATIENTGROUP'] 			= 	'Patient Group';
$lang['MEDICALGROUP'] 			= 	'Medical Group';
$lang['DATE'] 					= 	'Date';
$lang['TIME'] 					= 	'Time';
$lang['MESSAGE'] 				= 	'Message';
$lang['POSITION'] 				= 	'Position';
$lang['ADVERTISEMENT']			=	'Advertisement';
$lang['CHIEFCOMPLAINT']			=	'Chief Complaint';
$lang['OBSERVATION']			=	'Observation';
$lang['INVESTIGATION']			=	'Investigation';
$lang['DIAGNOSIS']				=	'Diagnosis';
$lang['NOTES']					=	'Notes';
$lang['ADVICE']					=	'Advice';
$lang['CLINICALNOTES']			=	'Clinical Notes';
$lang['PRESCRIPTION']			=	'Prescription';
$lang['MEDICINE']				=	'Medicine';
$lang['DURATION']				=	'Duration';
$lang['FREQUENCY']				=	'Frequency';
$lang['INSTRUCTIONS']			=	'Instructions';
$lang['QUANTITY']				=	'Quantity';
$lang['BILLING']				=	'Billing';
$lang['DISCHARGENOTE']			=	'Discharge Note';
$lang['CLINICALREPORT']			=	'Clinical Reports';
$lang['VITALS']					=	'Vitals';
$lang['HEIGHT']					=	'Height';
$lang['WEIGHT']					=	'Weight';
$lang['TEMPARATURE']			=	'Temperature';
$lang['BP']						=	'BP';
$lang['BMI']					=	'BMI';
$lang['KG']						=	'K.G';
$lang['CM']						=	'C.M';
$lang['BLOODGROUP']				=	'Blood Group';
$lang['BLOODSUGAR']				=	'Blood Sugar';
$lang['SMOKER']					=	'Smoker';
$lang['ALCOHOLIC']				=	'Alcoholic';
$lang['FOLLOWUP']				=	'Follow-up';
$lang['APPOINTMENT']			=	'Appointment';




$lang['BULKSMSADD'] 			= 	'Raise Request for Bulk SMS';


$lang['SEARCHPATIENT'] 			= 	'Search Patient';
$lang['ADVERTISEMENTIMAGE']		=	'Advertisement Image';
$lang['STARTDATE']				=	'Start Date';
$lang['ENDDATE']				=	'End Date';
$lang['ADVERTISEMENTTYPE']		=	'Advertisement Type';
$lang['TITLE']					=	'Title';
$lang['GENDER'] 				=	'Gender';
$lang['ADDRESS'] 				=	'Address';
$lang['UNIQUECODE']				=	'Patient Unique Code';
$lang['PATIENTGROUP']			=	'Patient Group';
$lang['LICENCENUMBER']			=	'Licence Number';
$lang['PROFILEIMAGE']			=	'Profile Image';
$lang['EDUCATION']				=	'Education';
$lang['STARTINGYEAROFPRACTICE']	=	'Starting Year of Practice';
$lang['LANGUAGES']				=	'Languages';
$lang['SPECIALITY']				=	'Speciality';
$lang['SERVICES']				=	'Services';
$lang['ABOUTDOCTOR']			=	'About Doctor';
$lang['HEADERIMAGE']			=	'Prescription Header Image';
$lang['FOOTERIMAGE']			=	'Prescription Footer Image';
$lang['SIGNATUREIMAGE']			=	'Doctor Digital Signature';
$lang['TIMESLOT']				=	'Time Slot';
$lang['CONSULTATIONFEE']		=	'Consultation Fee';
$lang['CONSULTATIONTYPE']		=	'Doctor Consultation Type';
$lang['FREEFOLLOWUPPERIOD']		=	'Free Followup Period';
$lang['REGISTRATION']			=	'Registration No';
$lang['UNIQUECLINICCODE']		=	'Unique Clinic Code';
$lang['CITY']					=	'City';
$lang['AREA']					=	'Area';
$lang['MAPURL']					=	'Map URL';
$lang['LANDLINE']				=	'Landline No';
$lang['EXTENSION']				=	'Extension';
$lang['YEARESTABLISHMENT']		=	'Year of Establishment';
$lang['PASSWORD']				=	'Password';
$lang['OLDPASSWORD']			=	'Old Password';
$lang['NEWPASSWORD']			=	'New Password';
$lang['CONFIRMPASSWORD']		=	'Confirm Password';
$lang['OTP']					=	'OTP';	
$lang['ROLE']					=	'Role';
$lang['YEARESTABLISHMENT']		=	'Year of Establishment';
$lang['SENDOTP']				=	'Send OTP';


//***** defined placeholders content for user dashboard *****//

$lang['NAME_PLACEHOLDER'] 				= 	'Enter Name';
$lang['EMAIL_PLACEHOLDER'] 				= 	'Enter Email';
$lang['MOBILE_PLACEHOLDER'] 			= 	'Enter Mobile Number';
$lang['AMOUNT_PLACEHOLDER'] 			= 	'Enter Amount';
$lang['MANUFACTURER_PLACEHOLDER'] 		= 	'Enter Manufacturer Name';
$lang['AVAILABLECOUNT_PLACEHOLDER'] 	= 	'Enter Available Count';
$lang['MINIMUMCOUNT_PLACEHOLDER'] 		= 	'Enter Minimum Count';
$lang['DISCOUNT_PLACEHOLDER'] 			= 	'Enter Discount';
$lang['TAX_PLACEHOLDER'] 				= 	'Enter Tax';
$lang['DESCRIPTION_PLACEHOLDER'] 		= 	'Enter Description';
$lang['DOB_PLACEHOLDER'] 				= 	'Enter Date of Birth';
$lang['AGE_PLACEHOLDER'] 				= 	'Select Age';
$lang['DATE_PLACEHOLDER'] 				= 	'Select Date';
$lang['TIME_PLACEHOLDER'] 				= 	'Select Time';
$lang['MESSAGE_PLACEHOLDER'] 			=	'Enter Message';
$lang['STARTDATE_PLACEHOLDER'] 			= 	'Select Start Date';
$lang['ENDDATE_PLACEHOLDER'] 			= 	'Select End Date';
$lang['ADDRESS_PLACEHOLDER'] 			= 	'Enter Address';
$lang['LICENCENUMBER_PLACEHOLDER'] 		= 	'Enter Licence Number';
$lang['EDUCATION_PLACEHOLDER'] 			= 	'Enter education';
$lang['CONSULTATIONFEE_PLACEHOLDER'] 	= 	'Enter Consultation Fee';
$lang['REGISTRATION_PLACEHOLDER']		=	'Enter Registration Number';
$lang['UNIQUECLINICCODE_PLACEHOLDER']	=	'Enter Unique Clinic Code';
$lang['MAPURL_PLACEHOLDER']				=	'Enter Map URL';
$lang['LANDLINE_PLACEHOLDER']			=	'Enter Landline Number';
$lang['EXTENSION_PLACEHOLDER']			=	'Enter Extension';
$lang['OLDPASSWORD_PLACEHOLDER']		=	'Enter Old Password';
$lang['NEWPASSWORD_PLACEHOLDER']		=	'Enter New Password';
$lang['CONFIRMPASSWORD_PLACEHOLDER']	=	'Enter Confirm Password';
$lang['OTP_PLACEHOLDER']				=	'Enter OTP';


$lang['CHIEFCOMPLAINT_PLACEHOLDER']		=	'Enter Chief Complaint';
$lang['OBSERVATION_PLACEHOLDER']		=	'Enter Observation';
$lang['INVESTIGATION_PLACEHOLDER']		=	'Enter Investigation';
$lang['DIAGNOSIS_PLACEHOLDER']			=	'Enter Diagnosis';
$lang['NOTES_PLACEHOLDER']				=	'Enter Notes';
$lang['ADVICE_PLACEHOLDER']				=	'Enter Advice';
$lang['INSTRUCTIONS_PLACEHOLDER']		=	'Enter Instructions';



//***** defined errors content for user dashboard *****//

$lang['NAME_ERROR'] 			= 	'Name Required';
$lang['EMAIL_ERROR'] 			= 	'Email Required';
$lang['MOBILE_ERROR'] 			= 	'Mobile Number Required';
$lang['MANUFACTURER_ERROR'] 	= 	'Manufacturer Name Required';
$lang['EXPENSESTYPE_ERROR'] 	= 	'Expense Type Required';
$lang['NUMBER_ERROR'] 			= 	'Enter Numbers Only';
$lang['PATIENTGROUP_ERROR'] 	= 	'Patient Group Required';
$lang['DATE_ERROR'] 			= 	'Date Required';
$lang['TIME_ERROR'] 			= 	'Time Required';
$lang['MESSAGE_ERROR'] 			= 	'Message Required';

$lang['POSITION_ERROR'] 		= 	'Position Required';
$lang['IMAGE_ERROR']			=	'Image Required';

$lang['ADDRESS_ERROR']			=	'Address Required';
$lang['ADVERTISEMENTIMAGE_ERROR']	=	'Advertisement Image Required';
$lang['STARTDATE_ERROR'] 		= 	'Start Date Required';
$lang['ENDDATE_ERROR'] 			= 	'End Date Required';
$lang['GENDER_ERROR'] 			= 	'Gender Required';
$lang['UNIQUECODE_ERROR']		=	'Patient Unique Code Required';
$lang['LICENCENUMBER_ERROR'] 	= 	'Licence Number Required';
$lang['TIMESLOT_ERROR'] 		= 	'Time Slot Required';
$lang['CONSULTATIONFEE_ERROR'] 	= 	'Consultation Fee Required';
$lang['DURATION_ERROR'] 		= 	'Duration Required';
$lang['FREQUENCY_ERROR'] 		= 	'Frequency Required';
$lang['INSTRUCTIONS_ERROR'] 	= 	'Instructions Required';
$lang['UNIQUECLINICCODE_ERROR']	=	'Unique Clinic Code Required';
$lang['CITY_ERROR']				=	'City Required';
$lang['AREA_ERROR']				=	'Area Required';
$lang['OLDPASSWORD_ERROR']		=	'Old Password Required';
$lang['NEWPASSWORD_ERROR']		=	'New Password Required';
$lang['CONFIRMPASSWORD_ERROR']	=	'Confirm Password Required';
$lang['OTP_ERROR']				=	'OTP Required';


?>