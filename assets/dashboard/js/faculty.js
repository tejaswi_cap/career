var currentPath = $("#currentPath").val();
$(document).ready(function () {
    "use strict";

    //***** Add faculty Form Validation *****//
    $('#facultyForm').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) { 
            e.preventDefault();
            var params = $.extend({}, doAjax_params_default);
            params['url'] = currentPath+'savefaculty';
            params['successCallbackFunction'] = facultyData;
            doAjaxForm(params, "#facultyForm");
        }
    });

    $(document).on('click', '.deleteFaculty', function(){

        var id = $(this).data("id");

        var params = $.extend({}, doAjax_params_default);
        params['url'] = currentPath+"deletefaculty";
        params['data'] = { id: id};
        params['successCallbackFunction'] = deleteFaculty;
        doAjax(params);
        
    });

});

function facultyData(data) {
    if(data.response == "Success") {
        toastr["success"](data.message);
        window.location.href = currentPath;
    } else {
        toastr["error"](data.message);
    }
}
function deleteFaculty(data) {

    if(data.status == "Success") {

        toastr["success"](data.message);
        window.location.href = currentPath;
    } else {
        toastr["error"](data.message);
    }

}
