var currentPath = urlPath+'admin/colleges/';
$(document).ready(function () {
    "use strict";
    $("[name='college_type_id']").append('<option value="quick_add">Add New</option>');
    $(document).on("change", "#field-college_type_id", function (e) {
        console.log('yes');
        
        var element = $(this).find(":selected").val();
        console.log(element);
        if(element == 'quick_add'){
            $('#field-college_type_id').val('').trigger("chosen:updated");
            $("#college_type_tag").remove();
            $('#field-college_type_id').parent().append('<div id="college_type_tag" class="m-t-10"><input type="text" id="newTag"></input><button type="button" id="submitNewTag">Add new option</button></div>');
        }else{
            $("#college_type_tag").remove();
        }

    });

    $(document).on("click", "#submitNewTag", function (e) {
        var name = $("input#newTag").val();

        $.ajax({
            url: currentPath+"quick_add_save",
            type: 'POST',
            dataType: "json",
            data: { name:name },
            success: function(data) {
                console.log(data);
                if(data.status == 'success'){
                    $('#field-college_type_id').append('<option value="'+data.id+'">'+data.name+'</option>').val(data.id);
                    $("#field-college_type_id").trigger("chosen:updated");
                    $("#college_type_tag").remove();

                }else{
                    toastr["error"](data.message);
                    $("#college_type_tag").remove();
                }
            }
        });

    });

});

