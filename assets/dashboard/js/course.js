var currentPath = $("#currentPath").val();
$(document).ready(function () {
    "use strict";

    //***** Add course Form Validation *****//
    $('#courseForm').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) { 
            e.preventDefault();
            var params = $.extend({}, doAjax_params_default);
            params['url'] = currentPath+'savecourse';
            params['successCallbackFunction'] = courseData;
            doAjaxForm(params, "#courseForm");
        }
    });

    $(document).on('click', '.deleteCourse', function(){

        var id = $(this).data("id");
        var params = $.extend({}, doAjax_params_default);
        params['url'] = currentPath+"deletecourse";
        params['data'] = { id: id };
        params['successCallbackFunction'] = deleteCourse;
        doAjax(params);
        
    });

});

function courseData(data) {
    if(data.response == "Success") {
        toastr["success"](data.message);
        window.location.href = currentPath;
    } else {
        toastr["error"](data.message);
    }
}
function deleteCourse(data) {

    if(data.status == "Success") {

        toastr["success"](data.message);
        window.location.href = currentPath;
    } else {
        toastr["error"](data.message);
    }

}
