var currentPath = $("#currentPath").val();
$(document).ready(function () {
    "use strict";

    //***** Add placement Form Validation *****//
    $('#placementForm').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) { 
            e.preventDefault();
            var params = $.extend({}, doAjax_params_default);
            params['url'] = currentPath+'saveplacement';
            params['successCallbackFunction'] = placementData;
            doAjaxForm(params, "#placementForm");
        }
    });

    $(document).on('click', '.deletePlacement', function(){

        var id = $(this).data("id");
        var params = $.extend({}, doAjax_params_default);
        params['url'] = currentPath+"deleteplacement";
        params['data'] = { id: id };
        params['successCallbackFunction'] = deletePlacement;
        doAjax(params);
        
    });

});

function placementData(data) {
    if(data.response == "Success") {
        toastr["success"](data.message);
        window.location.href = currentPath;
    } else {
        toastr["error"](data.message);
    }
}
function deletePlacement(data) {

    if(data.status == "Success") {

        toastr["success"](data.message);
        window.location.href = currentPath;
    } else {
        toastr["error"](data.message);
    }

}
