var currentPath = $("#currentPath").val();
$(document).ready(function () {
    "use strict";

    //***** Change Password Form Validation *****//
    $('#changeNameForm').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            var params = $.extend({}, doAjax_params_default);
            params['url'] = currentPath+'editprofile';
            params['successCallbackFunction'] = updateNameData;
            doAjaxForm(params, "#changeNameForm");
        }
    });

    //***** Change Password Form Validation *****//
    $('#changePasswordForm').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            var params = $.extend({}, doAjax_params_default);
            params['url'] = currentPath+'editpassword';
            params['successCallbackFunction'] = updatePasswordData;
            doAjaxForm(params, "#changePasswordForm");
        }
    });

    //***** Change Email Form Validation *****//
    $('#changeEmailForm').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            var params = $.extend({}, doAjax_params_default);
            params['url'] = currentPath+'editemail';
            params['successCallbackFunction'] = updateEmailData;
            doAjaxForm(params, "#changeEmailForm");
        }
    });

    $(document).on('click', '.sendEmailOTP', function(){

        var params = $.extend({}, doAjax_params_default);
        params['url'] = currentPath+"sendotp";
        params['data'] = {};
        params['successCallbackFunction'] = emailOtpData;
        doAjax(params);
        
    });

    $(".select2").select2();

    //***** Update Profile Form Validation *****//
    $('#profileForm').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            var params = $.extend({}, doAjax_params_default);
            params['url'] = currentPath+'editprofile';
            params['successCallbackFunction'] = updateCollegeData;
            doAjaxForm(params, "#profileForm");
        }
    });

    //***** Update Addition Profile Form Validation *****//
    $('#profileAddForm').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            var params = $.extend({}, doAjax_params_default);
            params['url'] = currentPath+'editprofileinfo';
            params['successCallbackFunction'] = updateCollegeAddData;
            doAjaxForm(params, "#profileAddForm");
        }
    });

    //***** Update Facility Form Validation *****//
    $('#facilityForm').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            var params = $.extend({}, doAjax_params_default);
            params['url'] = currentPath+'editfacility';
            params['successCallbackFunction'] = updateFacilityData;
            doAjaxForm(params, "#facilityForm");
        }
    });

    $(".dropify").dropify();

    $('#brochureForm').validator().on('change', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            var params = $.extend({}, doAjax_params_default);
            params['url'] = currentPath+'editbrochure';
            params['successCallbackFunction'] = updateBrochureData;
            doAjaxForm(params, "#brochureForm");
        }
    });

});


function emailOtpData(data) {

    if(data.status == "Success") {
        $(".emailOTPDiv").show();
        $(".sendEmailButton").hide();
        toastr["success"](data.message);
    } else {
        toastr["error"](data.message);
    }

}

function updateNameData(data) {

    if(data.response == "Success") {
        $(".editableName").text(data.name);
        $(".editableMobile").text(data.mobile);
        $(".editableLocation").text(data.location);
        rightSlidebarAction("#changeNameSideBar");
        toastr["success"](data.message);
    } else {
        toastr["error"](data.message);
    }

}

function updatePasswordData(data) {
    if(data.response == 1) {
        toastr["success"](data.message);
        rightSlidebarAction("#changePasswordSideBar");
    } else {
        toastr["error"](data.message);
    }
}

function updateEmailData(data) {
    if(data.response == "Success") {
        $(".editableEmail").text(data.email);
        rightSlidebarAction("#changeEmailSideBar");
        toastr["success"](data.message);
    } else {
        toastr["error"](data.message);
    }
}

function emailUpdateFormReset() {
    $(".emailOTPDiv").hide();
    $(".sendEmailButton").show();

}

function updateCollegeData(data) {

    if(data.response == "Success") {
        toastr["success"](data.message);
    } else {
        toastr["error"](data.message);
    }

}

function updateCollegeAddData(data) {

    if(data.response == "Success") {
        toastr["success"](data.message);
    } else {
        toastr["error"](data.message);
    }

}

function updateFacilityData(data) {

    if(data.response == "Success") {
        toastr["success"](data.message);
    } else {
        toastr["error"](data.message);
    }

}

function updateBrochureData(data) {

    if(data.response == "Success") {
        toastr["success"](data.message);
    } else {
        toastr["error"](data.message);
    }

}