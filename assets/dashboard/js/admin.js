var currentPath = $("#currentPath").val();
$(document).ready(function () {

    "use strict";

    //***** Admin login Form Validation *****//
    $('#loginForm').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) { //what is event here?
            e.preventDefault();
            var params = $.extend({}, doAjax_params_default);
            params['url'] = currentPath+'login';
            params['successCallbackFunction'] = loginResponseData;
            doAjaxForm(params, "#loginForm");
        }
    });

    //***** Admin login Form Validation *****//
    $('#forgotPasswordForm').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            var params = $.extend({}, doAjax_params_default);
            params['url'] = currentPath+'resetpassword';
            params['successCallbackFunction'] = resetPasswordResponseData;
            doAjaxForm(params, "#forgotPasswordForm");
        }
    });

    $("#to-recover").on("click",function(){
        $("#loginForm").slideUp(),$("#forgotPasswordForm").fadeIn()
    });
    $("#to-login").on("click",function(){
        $("#forgotPasswordForm").slideUp(),$("#loginForm").slideDown()
    });

    
});

function loginResponseData(data) {

    if(data.status == "Success") {
        toastr["success"](data.message);
        window.location.href = currentPath+"dashboard";
    } else {
        toastr["error"](data.message);
    }

}

function resetPasswordResponseData(data) {

    if(data.status == "Success") {
        toastr["success"](data.message);
        $("#to-login").click();
    } else {
        toastr["error"](data.message);
    }

}