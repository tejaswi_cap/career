$(document).ready(function () {

    "use strict";

    $(document).on('click', '#buttonFilter', function(){

        var doctor = $("#doctorIdSearch").val();
        var date = $("#dateSearch").val();
        var params = {
            'doctorId' : doctor,
            'date' : date
        }
        dataTableInitializationCustomFilter("dataTable", [0], "reports", "cashflowdata", params);
        $("#buttonFilterClear").show();

    });

    $(document).on('click', '#buttonFilterClear', function(){

        $("#doctorIdSearch").val("").selectpicker("refresh");
        $("#statusSearch").val("").selectpicker("refresh");
        $("#dateSearch").val("").daterangepicker("refresh");
        dataTableInitializationCustomFilter("dataTable", [0], "reports", "cashflowdata");
        $("#buttonFilterClear").hide();

    });
    
});