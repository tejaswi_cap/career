
var urlPath = $("#urlPath").val();
var body = $("body");
var clinicPatientData = [];
var patientFamilyData = [];

var substringMatcher = function(strs) {
    return function findMatches(q, cb) {

        var matches, substringRegex;
        matches = [];
        substrRegex = new RegExp(q, 'i');
        $.each(strs, function(i, str) {
          if (substrRegex.test(str)) {
            matches.push(str);
          }
        });
        cb(matches);
    };
};



$(document).ready(function () {
    "use strict";

    $('.dropify').dropify();


    $('.reportrange').daterangepicker();
    // $('.reportrange').val("");

    $('.clockpicker').clockpicker({
        donetext: 'Done',
    }).find('input').change(function() {
        console.log(this.value);
    });

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        startDate: new Date(),
        autoclose: true
    });
    $('.datepickerDob').datepicker({
        format: 'yyyy-mm-dd',
        endDate: new Date(),
        autoclose: true    
    });

    $('.selectpicker').selectpicker();

    $('.selectpicker').on( 'hide.bs.select', function ( ) {
        $(this).trigger("focusout");
    });

    $(".counter").counterUp({
        delay: 100,
        time: 1200
    });


    $(document).on('click', '.formSidebar', function(){
        var id = $(this).data("value");
        var title = $(this).data("title");
        var callBackFunction = $(this).data("function");
        if (typeof callBackFunction !== "undefined") {
            var fn = window[callBackFunction];
            if (typeof fn === "function") {
                fn();
            }
        }
        $("#"+id+"Form #id").val(0);
        $("#"+id+"Form").trigger("reset");
        if (typeof title !== "undefined") {
            rightSlidebarAction("#"+id+"SideBar", title);
        } else {
            rightSlidebarAction("#"+id+"SideBar");
        };

    });

    $(document).on('click', '.formSidebarWithoutReset', function(){

        var id = $(this).data("value");
        var title = $(this).data("title");
        var callBackFunction = $(this).data("function");
        if (typeof callBackFunction !== "undefined") {
            var fn = window[callBackFunction];
            if (typeof fn === "function") {
                fn();
            }
        }
        if (typeof title !== "undefined") {
            rightSlidebarAction("#"+id+"SideBar", title);
        } else {
            rightSlidebarAction("#"+id+"SideBar");
        };
        
    });


    $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
        event.preventDefault();
        return $(this).ekkoLightbox({
            onShown: function() {
                if (window.console) {
                    return console.log('Checking our the events huh?');
                }
            },
            onNavigate: function(direction, itemIndex) {
                if (window.console) {
                    return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                }
            }
        });
    });

});


function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    hours = hours < 10 ? '0'+hours : hours;
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

//*** Date Picker ***//
$(document).ready(function () {

    $('#startdate').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        startDate : new Date()
    }).on("changeDate", function(selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#enddate').datepicker('setStartDate', minDate);
        $(this).parent().find("label.error").remove();
        $(this).removeClass("error");   
    });


    $('#enddate').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        startDate : new Date()
    }).on('changeDate', function (selected) {
        var maxDate = new Date(selected.date.valueOf());
        $('#startdate').datepicker('setEndDate', maxDate);
        $(this).parent().find("label.error").remove();
        $(this).removeClass("error");  
    });


    var sDate = $("#startdate").val();
    if(sDate != "") {
        var minDate = new Date(sDate);
        $('#enddate').datepicker('setStartDate', minDate);
    } 

    var eDate = $("#enddate").val();
    if(eDate != "") {
        var maxDate = new Date(eDate);
        $('#startdate').datepicker('setEndDate', maxDate);
    }

    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });

    $('.dataTableSimple').dataTable({
        "pageLength" : 10,
        "bDestroy" : true,
        "bAutoWidth" : true,  
        "bFilter" : true,
        "bSort" : true, 
        "processing" :false,
        "serverSide" :false
    });


});

$.fn.dataTable.pipeline = function ( opts ) {
    var conf = $.extend( {
        url: '',      
        controllerName: '',      
        data: null,   
        method: 'POST' 
    }, opts );
 
    var cacheLower = -1;
    var cacheUpper = null;
    var cacheLastRequest = null;
    var cacheLastJson = null;
 
    return function ( request, drawCallback, settings ) {
        var ajax          = false;
        var requestStart  = request.start;
        var drawStart     = request.start;
        var requestLength = request.length;
        var requestEnd    = requestStart + requestLength;
        var controllerName = '';
        var rowData = '';
        var tableData = [];
         
        if ( settings.clearCache ) {
            ajax = true;
            settings.clearCache = false;
        }
        else if ( cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper ) {
            ajax = true;
        }
        else if ( JSON.stringify( request.order )   !== JSON.stringify( cacheLastRequest.order ) ||
                  JSON.stringify( request.columns ) !== JSON.stringify( cacheLastRequest.columns ) ||
                  JSON.stringify( request.search )  !== JSON.stringify( cacheLastRequest.search )
        ) {
            ajax = true;
        }
         
        cacheLastRequest = $.extend( true, {}, request );
 
        if ( ajax ) {
            if ( requestStart < cacheLower ) {
                requestStart = requestStart - (requestLength*(conf.pages-1));
 
                if ( requestStart < 0 ) {
                    requestStart = 0;
                }
            }
             
            cacheLower = requestStart;
            cacheUpper = requestStart + (requestLength * conf.pages);
 
            request.start = requestStart;
            request.length = requestLength*conf.pages;
            controllerName = conf.controllerName;
 
            if ( typeof conf.data === 'function' ) {
                var d = conf.data( request );
                if ( d ) {
                    $.extend( request, d );
                }
            }
            else if ( $.isPlainObject( conf.data ) ) {
                $.extend( request, conf.data );
            }
 
            settings.jqXHR = $.ajax( {
                "type":     conf.method,
                "url":      conf.url,
                "data":     request,
                "dataType": "json",
                "cache":    false,
                "success":  function ( json ) {
                    loadJSON(function(jsonData) {
                        var urlArray = conf.url.split("/");
                        var urlMethod = urlArray[urlArray.length-1];
                        if(urlMethod == "gettabledata") {
                            rowData = JSON.parse(jsonData)[controllerName];
                        } else {
                            rowData = JSON.parse(jsonData)[controllerName+"/"+urlMethod];
                        }
                        
                        if(requestStart) {
                            tableData = loadTableData(json.records, rowData, requestStart);
                        } else {
                            tableData = loadTableData(json.records, rowData, 0);
                        }
                        
                        json.data =  tableData;
                        cacheLastJson = $.extend(true, {}, json);
                        if ( cacheLower != drawStart ) {
                            json.data.splice( 0, drawStart-cacheLower );
                        }
                        if ( requestLength >= -1 ) {
                            json.data.splice( requestLength, json.data.length );
                        }

                        if(typeof json.creditAmount !== "undefined") {
                            $(".creditAmount").html(json.creditAmount)
                        }
                        if(typeof json.debitAmount !== "undefined") {
                            $(".debitAmount").html(json.debitAmount)
                        }
                        if(typeof json.totalAmount !== "undefined") {
                            $(".totalAmount").html(json.totalAmount)
                        }
                        drawCallback( json );

                    });
                    
                }
            } );
        } else {
            json = $.extend( true, {}, cacheLastJson );
            json.draw = request.draw; 
            json.data.splice( 0, requestStart-cacheLower );
            json.data.splice( requestLength, json.data.length );
 
            drawCallback(json);
        }
    }
};
 
$.fn.dataTable.Api.register( 'clearPipeline()', function () {
    return this.iterator( 'table', function ( settings ) {
        settings.clearCache = true;
    });
});

$.extend($.fn.dataTable.defaults, {
    language: {
        "processing": "Loading..."
    },
});


function dataTableInitialization(tableId, columnsArray, controllerName, methodName = "") {

    if (methodName == "") {
        methodName = "gettabledata";
    }
    if(document.getElementsByClassName(controllerName+'Datatable').length > 0) {
        $("."+controllerName+'Datatable').dataTable({
            "pageLength" : 10,
            "bDestroy" : true,
            "bAutoWidth" : true,  
            "bFilter" : true,
            "bSort" : true, 
            "processing" :true,
            "serverSide" :true,
            "order" : [],
            "sServerMethod" : 'POST',
            "aoColumnDefs" : [
                {
                    'bSortable' : false,
                    'aTargets' : columnsArray
                }
            ],
            fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                var rowId = $.trim($(aData[0]).text());
                var rowClass = "rowDatadetails"+  rowId;
                $(nRow).addClass(rowClass);
            },
            "ajax" : $.fn.dataTable.pipeline({
                "url" : urlPath + "user/" + controllerName + "/"+methodName,
                "pages" : 5,
                "controllerName" : controllerName
            })
        });

        // search works only we click enter key
        var tableApi = $("#"+tableId).dataTable().api();
        $("#"+tableId+"_filter label input").unbind().bind("keyup", function(e) {
            if(e.keyCode == 13) {
                tableApi.search(this.value).draw();
            }
            if(this.value == "") {
                tableApi.search("").draw();
            }
            return;
        });
    }
    
}



function dataTableInitializationSimple(tableId, columnsArray, controllerName, methodName = "", tableClass = "") {

    if (methodName == "") {
        methodName = "gettabledata";
    }
    if(tableClass == "") {
        tableClass = controllerName;
    }
    if(document.getElementsByClassName(tableClass+'Datatable').length > 0) {
        $("."+tableClass+'Datatable').dataTable({
            "pageLength" : 10,
            "bDestroy" : true,
            "bAutoWidth" : true,  
            "bFilter" : true,
            "bSort" : true, 
            "processing" :false,
            "serverSide" :false,
            "order" : [],
            "sServerMethod" : 'POST',
            "aoColumnDefs" : [
                {
                    'bSortable' : false,
                    'aTargets' : columnsArray
                }
            ],
            fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                var rowId = $.trim($(aData[0]).text());
                var rowClass = "rowDatadetails"+  rowId;
                $(nRow).addClass(rowClass);
            },
            "ajax" : $.fn.dataTable.pipeline({
                "url" : urlPath + "user/" + controllerName + "/"+methodName,
                "controllerName" : controllerName
            })
        });

    }
    
}


function dataTableInitializationCustomFilter(tableId, columnsArray, controllerName, methodName = "", params = {}) {

    if (methodName == "") {
        methodName = "gettabledata";
    }
    if(document.getElementsByClassName(controllerName+'Datatable').length > 0) {
        var datatableCustomFilter = $("."+controllerName+'Datatable').dataTable({
            "pageLength" : 10,
            "bDestroy" : true,
            "bAutoWidth" : true,  
            "bFilter" : true,
            "bSort" : true, 
            "processing" :true,
            "serverSide" :true,
            "order" : [],
            "sServerMethod" : 'POST',
            "aoColumnDefs" : [
                {
                    'bSortable' : false,
                    'aTargets' : columnsArray
                }
            ],
            fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                var rowId = $.trim($(aData[0]).text());
                var rowClass = "rowDatadetails"+  rowId;
                $(nRow).addClass(rowClass);
            },
            "ajax" : $.fn.dataTable.pipeline({
                "url" : urlPath + "user/" + controllerName + "/"+methodName,
                "pages" : 5,
                "controllerName" : controllerName,
                'data' : params
            })
        });

        // search works only we click enter key
        var tableApi = $("#"+tableId).dataTable().api();
        $("#"+tableId+"_filter label input").unbind().bind("keyup", function(e) {
            if(e.keyCode == 13) {
                tableApi.search(this.value).draw();
            }
            if(this.value == "") {
                tableApi.search("").draw();
            }
            return;
        });

    }
    
}

function loadJSON(callback) {   

    var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
    xobj.open('GET', urlPath+'assets/dashboard/js/datatable.json', true); // Replace 'my_data' with the path to your file
    xobj.onreadystatechange = function () {
          if (xobj.readyState == 4 && xobj.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(xobj.responseText);
          }
    };
    xobj.send(null);  
}


function loadTableData(data, rowData, rowNo) {

    var requestData = [];
    for(var i=0; i<data.length; i++) {
        var newRowData = rowData;
        rowNo++;
        var old = JSON.stringify(newRowData).replace('{SNO}', rowNo); //convert to JSON string
        newRowData = JSON.parse(old);
        var singleRowData = data[i];
        for (var row in singleRowData) {
            var old = JSON.stringify(newRowData).replace(new RegExp('{'+row.toUpperCase()+'}', "gi"),singleRowData[row]); //convert to JSON string
            newRowData = JSON.parse(old);
            if(row.toUpperCase() == "ACTIONS") {
                newRowData.pop();
                newRowData.push(" ");
            }

        }

        requestData.push(newRowData);
    }
    return requestData;

}


function loadSimpleTableData(data, rowData, tableName) {
    var rowNo = 0;
    var requestData = [];
    $('#'+tableName+' tbody').empty();
    for(var i=0; i<data.length; i++) {
        var newRowData = rowData;
        rowNo++;
        var old = JSON.stringify(newRowData).replace('{SNO}', rowNo); //convert to JSON string
        newRowData = JSON.parse(old);
        var singleRowData = data[i];
        for (var row in singleRowData) {
            if(row.toUpperCase() == "ID") {
                var old = JSON.stringify(newRowData).replace(/{ID}/g,singleRowData[row]); //convert to JSON string
            } else {
                var old = JSON.stringify(newRowData).replace('{'+row.toUpperCase()+'}',singleRowData[row]); //convert to JSON string
            }
            newRowData = JSON.parse(old);
            if(row.toUpperCase() == "ACTIONS") {
                newRowData.pop();
                newRowData.push(" ");
            }

        }
        requestData.push(newRowData);
        // drawTable(requestData, tableName);
    }
    var count = requestData.length;
    for(var i=0;i<count;i++){
        var response = requestData[i];
        var table = document.getElementById(tableName).getElementsByTagName('tbody')[0];
        var row = table.insertRow(-1);
        for(var j = 0; j<response.length; j++) {
            var col = row.insertCell(j);
            col.innerHTML = response[j];
        }
    }
    return requestData;

}

var doAjax_params_default = {
    'url': null,
    'requestType': "POST",
    'contentType': 'application/x-www-form-urlencoded; charset=UTF-8',
    'dataType': 'json',
    'data': {},
    'beforeSendCallbackFunction': null,
    'successCallbackFunction': null,
    'completeCallbackFunction': null,
    'errorCallBackFunction': null,
};


function doAjax(doAjax_params) {

    var url = doAjax_params['url'];
    var requestType = doAjax_params['requestType'];
    var contentType = doAjax_params['contentType'];
    var dataType = doAjax_params['dataType'];
    var data = doAjax_params['data'];
    var beforeSendCallbackFunction = doAjax_params['beforeSendCallbackFunction'];
    var successCallbackFunction = doAjax_params['successCallbackFunction'];
    var completeCallbackFunction = doAjax_params['completeCallbackFunction'];
    var errorCallBackFunction = doAjax_params['errorCallBackFunction'];
    $.ajax({
        url: url,
        crossDomain: true,
        type: requestType,
        contentType: contentType,
        dataType: dataType,
        data: data,
        beforeSend: function(jqXHR, settings) {

        },
        success: function(data, textStatus, jqXHR) {    
            if (typeof successCallbackFunction === "function") {
                successCallbackFunction(data);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            if (typeof errorCallBackFunction === "function") {
                errorCallBackFunction(errorThrown);
            }

        },
        complete: function(jqXHR, textStatus) {

        }
    });
}


function doAjaxForm(doAjax_params, formName) {

    var url = doAjax_params['url'];
    var requestType = doAjax_params['requestType'];
    var dataType = doAjax_params['dataType'];
    var form = $(formName)[0];
    var beforeSendCallbackFunction = doAjax_params['beforeSendCallbackFunction'];
    var successCallbackFunction = doAjax_params['successCallbackFunction'];
    var completeCallbackFunction = doAjax_params['completeCallbackFunction'];
    var errorCallBackFunction = doAjax_params['errorCallBackFunction'];

    $.ajax({
        url: url,
        crossDomain: true,
        type: requestType,
        contentType: false,
        dataType: dataType,
        data: new FormData(form),
        processData:false,
        beforeSend: function(jqXHR, settings) { //called before sending request
            $(formName+" .formAlertDiv .alert").show();
            $('button[type=submit], input[type=submit]').prop('disabled',true);
        },
        success: function(data, textStatus, jqXHR) {    //called when a success code is returned 
            if (typeof successCallbackFunction === "function") {
                $(formName+" .formAlertDiv .alert").hide();
                successCallbackFunction(data);
                $('button[type=submit], input[type=submit]').prop('disabled',false);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) { //called when a error is returned
            if (typeof errorCallBackFunction === "function") {
                $(formName+" .formAlertDiv .alert").hide();
                errorCallBackFunction(errorThrown);
                $('button[type=submit], input[type=submit]').prop('disabled',false);
            }

        },
        complete: function(jqXHR, textStatus) {  //called on completion of request
            if (typeof completeCallbackFunction === "function") {
                 $(formName+" .formAlertDiv .alert").hide();
                completeCallbackFunction();
                $('button[type=submit], input[type=submit]').prop('disabled',false);
            }
        }
    });
}


toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "rtl": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": 300,
    "hideDuration": 1000,
    "timeOut": 5000,
    "extendedTimeOut": 1000,
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};


var doAlert_delete_params_default = {
    'data' : {},
    'title' : "Are you sure?",
    'text' : "You will not be able to recover this data!",
    'type' : "warning",
    'confirmButtonText' : 'Yes, delete it!',
    'cancelButtonText' : 'No',
    'confirmButtonColor' : "#DD6B55",
    'successCallbackFunction': null
};


var doAlert_status_params_default = {
    'data' : {},
    'title' : "Are you sure?",
    'text' : "Do you want change the status!",
    'type' : "warning",
    'confirmButtonText' : 'Yes',
    'cancelButtonText' : 'No',
    'confirmButtonColor' : "#1E88E5",
    'successCallbackFunction': null
};


function doAlert(params) {

    var successCallbackFunction = params['successCallbackFunction'];
    swal({ 
        title: params['title'],
        text: params['text'],
        type: params['type'],
        showCancelButton: true,
        confirmButtonText: params['confirmButtonText'],
        cancelButtonText: params['cancelButtonText'],
        closeOnConfirm: false,
        confirmButtonColor : params['confirmButtonColor'],
        allowOutsideClick: false,
        allowEscapeKey: false
    },
    function() {        
        swal.disableButtons(); 
        window.onkeydown = null;
        window.onfocus = null; 
        if (typeof successCallbackFunction === "function") {
            successCallbackFunction(params['data']);
        }
    });
}


function viewModal(data, modal, modalType) {

    var i;
    var count = Object.keys(data).length;
    for(i=0;i<count;i++){
        var key = Object.keys(data)[i];
        if(modalType == 'edit') {
            $(modal+' #'+key).val(data[key]);
        } else {
            $('.'+key).html(data[key]);
        }
        
    }
    $(modal).modal("show");
}


function viewSideBar(data, sidebar, title = "") {

    var i;
    var count = Object.keys(data).length;
    for(i=0;i<count;i++){
        var key = Object.keys(data)[i];
        $(sidebar+' [name="'+key+'"]').val(data[key]);
    }
    rightSlidebarAction(sidebar, title);

}

function loadData(data, parentId) {

    var i;
    var count = Object.keys(data).length;
    for(i=0;i<count;i++){
        var key = Object.keys(data)[i];
        $(parentId+' [name="'+key+'"]').val(data[key]);
    }

}

function loadDisabledData(data, parentId) {

    var i;
    var count = Object.keys(data).length;
    for(i=0;i<count;i++){
        var key = Object.keys(data)[i];
        $(parentId+' input[name="'+key+'"]').val(data[key]);
        if(data[key] != "" && data[key] != null ) {
            $(parentId+' input[name="'+key+'"]').attr('readonly', true);
        }
    }

}

/* ===== Open-Close Right Sidebar ===== */
function rightSlidebarAction(id, title = "") {
    if(title != "") {
        $(id+" .rpanel-title h4")[0].childNodes[0].nodeValue = title;
    }
    $('.selectpicker').selectpicker('refresh');
    $(id).slideDown(50).toggleClass("shw-rside");
    $(".fxhdr").on("click", function () {
        body.toggleClass("fix-header"); /* Fix Header JS */
    });
    $(".fxsdr").on("click", function () {
        body.toggleClass("fix-sidebar"); /* Fix Sidebar JS */
    });
    /* ===== Service Panel JS ===== */
    var fxhdr = $('.fxhdr');
    if (body.hasClass("fix-header")) {
        fxhdr.attr('checked', true);
    } else {
        fxhdr.attr('checked', false);
    }
}

/* ===== Load Select Options Dynamically ===== */

function loadSelectOptions(selectType, data, selectedValue = "") {

    var optionData = "";
    $.each(data, function (index, value) {
        optionData += '<option value="' + value.id + '">' + value.name + '</option>';
    });
    $(selectType).html(optionData).selectpicker('refresh');
    $(selectType).val(selectedValue).selectpicker('refresh');
}

