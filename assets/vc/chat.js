/**
* Copyright 2018 Google Inc. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
'use strict';

// Signs-in Friendly Chat.
var patientId = $("#patientUID").val();
var doctorId = $("#doctorUID").val();
var userType = "";

function signIn() {

    // alert('TODO: Implement Google Sign-In');
    // var provider = new firebase.auth.GoogleAuthProvider();
    // firebase.auth().signInWithPopup(provider);
    userType = $("#userType").val();
    var patientName = $("#patientName").val();
    var doctorName = $("#doctorName").val();
    var appointmentId = $("#appointmentId").val();
    var email = appointmentId+"patient@cap.com";
    //Create Patient with Email and Password

    firebase.auth().signInWithEmailAndPassword(email, email)
            .then(function() { 
                var user = firebase.auth().currentUser;
            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(errorMessage);
                // ...
            });
        loadMessages();

}

// Signs-out of Friendly Chat.
function signOut() {
    // TODO 2: Sign out of Firebase.
    firebase.auth().signOut();
}

// Initiate firebase auth.
function initFirebaseAuth() {
    // TODO 3: Initialize Firebase.
    firebase.auth().onAuthStateChanged(authStateObserver);
}

// Returns the signed-in user's profile Pic URL.
function getProfilePicUrl() {
    // TODO 4: Return the user's profile pic URL.
    return firebase.auth().currentUser.photoURL || '/images/profile_placeholder.png';
}

// Returns the signed-in user's display name.
function getUserName() {
    // TODO 5: Return the user's display name.
    return firebase.auth().currentUser.displayName;
}

// Returns true if a user is signed-in.
function isUserSignedIn() {
    // TODO 6: Return true if a user is signed-in.
    return !!firebase.auth().currentUser;
}

function getMonth() {
    const date = new Date();
    const dateTimeFormat = new Intl.DateTimeFormat('en', { year: 'numeric', month: 'short', day: '2-digit' }) 
    const [{ value: month },,{ value: day },,{ value: year }] = dateTimeFormat .formatToParts(date ) 

    return `${month} ${day}, ${year }`;
}

function getTime() {

    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;

}

// Saves a new message on the Firebase DB.
function saveMessage(messageText) {


    var sendDate = getMonth();
    var sendTime = getTime();

    var timestamp = firebase.database.ServerValue.TIMESTAMP;

    if(userType == 'USER') {
        var fromId = patientId;
        var toId = doctorId;
    } else {
        var fromId = doctorId;
        var toId = patientId;
    }

    var key = firebase.database().ref('Messages' + "/" + patientId + "/" + doctorId).push().key;
    return firebase.database().ref('Messages' + "/" + patientId + "/" + doctorId + '/'+key)
    .set({
        date: sendDate,
        from: fromId,
        message: messageText,
        time: sendTime,
        to: toId,
        type: "text",
        timestamp : timestamp
    }).catch(function(error) {
        console.error('Error writing new message to database', error);
    });
}

// Loads chat messages history and listens for upcoming ones.
function loadMessages() {
    // TODO 8: Load and listens for new messages.
    
    var chatRef =  firebase.database().ref('Messages' + "/" + patientId + "/" + doctorId);

    chatRef.once('value').then(function(snapshot) {
        // console.log(snapshot);
        snapshot.forEach(function(change) {
            var key = change.key;
            console.log(key);
            var message = change.val();
            var from = message.from;
            if(from == doctorId) {
                var messageType = "outgoing";
            } else {
                var messageType = "incoming";
            }
            console.log(message.timestamp);
            displayMessage(key, message.date, message.time, message.message, message.timestamp, message.type, messageType, "");
        });
    }).catch(function(error) {
        console.error('error list', error);
    });

    chatRef.on('child_added', function(data) {
        var key = data.key;
        var message = data.val();
        var from = message.from;
        if(userType == 'USER') {
            var fromId = patientId;
        } else {
            var fromId = doctorId;
        }
        if(from == fromId) {
            var messageType = "outgoing";
        } else {
            var messageType = "incoming";
        }
        displayMessage(key, message.date, message.time, message.message, message.timestamp, message.type, messageType, "");
    });


}

// Saves a new message containing an image in Firebase.
// This first saves the image in Firebase storage.
function saveImageMessage(file) {
    // TODO 9: Posts a new image as a message.

    var sendDate = getMonth();
    var sendTime = getTime();
    var timestamp = firebase.database.ServerValue.TIMESTAMP;
    if(userType == 'USER') {
        var fromId = patientId;
        var toId = doctorId;
    } else {
        var fromId = doctorId;
        var toId = patientId;
    }

    var filePath = firebase.auth().currentUser.uid + '/' + file.name;
    return firebase.storage().ref(filePath).put(file).then(function(fileSnapshot) {
        return fileSnapshot.ref.getDownloadURL().then((url) => {
            var key = firebase.database().ref('Messages' + "/" + patientId + "/" + doctorId).push().key;
            return firebase.database().ref('Messages' + "/" + patientId + "/" + doctorId + '/'+key)
            .set({
                date: sendDate,
                from: fromId,
                message: url,
                time: sendTime,
                name: file.name,
                to: toId,
                type: "image",
                timestamp : timestamp
            }).catch(function(error) {
                console.error('Error writing new message to database', error);
            });
        });
    });
}


function savePdfMessage(file) {
    // TODO 9: Posts a new image as a message.

    var sendDate = getMonth();
    var sendTime = getTime();
    var timestamp = firebase.database.ServerValue.TIMESTAMP;
    if(userType == 'USER') {
        var fromId = patientId;
        var toId = doctorId;
    } else {
        var fromId = doctorId;
        var toId = patientId;
    }

    var filePath = firebase.auth().currentUser.uid + '/' + file.name;
    return firebase.storage().ref(filePath).put(file).then(function(fileSnapshot) {
        return fileSnapshot.ref.getDownloadURL().then((url) => {
            var key = firebase.database().ref('Messages' + "/" + patientId + "/" + doctorId).push().key;
            return firebase.database().ref('Messages' + "/" + patientId + "/" + doctorId + '/'+key)
            .set({
                date: sendDate,
                from: fromId,
                message: url,
                time: sendTime,
                name: file.name,
                to: toId,
                type: "pdf",
                timestamp : timestamp
            }).catch(function(error) {
                console.error('Error writing new message to database', error);
            });
        });
    });
}

// Saves the messaging device token to the datastore.
function saveMessagingDeviceToken() {
    // TODO 10: Save the device token in the realtime datastore
}

// Requests permissions to show notifications.
function requestNotificationsPermissions() {
    // TODO 11: Request permissions to send notifications.
}

// Triggered when a file is selected via the media picker.
function onMediaFileSelected(event) {
    event.preventDefault();
    var file = event.target.files[0];

    // Clear the selection in the file picker input.
    imageFormElement.reset();

    // Check if the file is an image.
    if (!file.type.match('image.*')) {
        var data = {
            message: 'You can only share images',
            timeout: 2000
        };
        // signInSnackbarElement.MaterialSnackbar.showSnackbar(data);
        return;
    }
    // Check if the user is signed-in
    if (checkSignedInWithMessage()) {
        saveImageMessage(file);
    }
}


// Triggered when a file is selected via the media picker.
function onPdfFileSelected(event) {
    event.preventDefault();
    var file = event.target.files[0];

    // Clear the selection in the file picker input.
    imageFormElement.reset();
    // Check if the file is an image.
    if (!file.type.match('pdf')) {
        var data = {
            message: 'You can only share images',
            timeout: 2000
        };
        // signInSnackbarElement.MaterialSnackbar.showSnackbar(data);
        return;
    }
    // Check if the user is signed-in
    if (checkSignedInWithMessage()) {
        savePdfMessage(file);
    }
}

// Triggered when the send new message form is submitted.

var requestRunning = false;
function onMessageFormSubmit(e) {

    e.preventDefault();
    // Check that the user entered a message and is signed in.
    if (messageInputElement.value && checkSignedInWithMessage()) {
        if(requestRunning) {
            return;
        }
        requestRunning = true;
        saveMessage(messageInputElement.value).then(function() {
            // Clear message text field and re-enable the SEND button.
            resetMaterialTextfield(messageInputElement);
            requestRunning = false;
            toggleButton();
        });
    }

}

// Triggers when the auth state change for instance when the user signs-in or signs-out.
function authStateObserver(user) {
    if (user) { 

        var profilePicUrl = getProfilePicUrl();
        var userName = getUserName();
        saveMessagingDeviceToken();

    } 
}

// Returns true if user is signed-in. Otherwise false and displays a message.
function checkSignedInWithMessage() {
    // Return true if the user is signed in Firebase
    if (isUserSignedIn()) {
        return true;
    }

    // Display a message to the user using a Toast.
    var data = {
        message: 'You must sign-in first',
        timeout: 2000
    };
    // signInSnackbarElement.MaterialSnackbar.showSnackbar(data);
    return false;
}


// Resets the given MaterialTextField.
function resetMaterialTextfield(element) {
    element.value = '';
    // element.parentNode.MaterialTextfield.boundUpdateClassesHandler();
}


// Template for messages.
var INCOMING_TEMPLATE =
'<div class="row message-body">'+
'<div class="col-sm-12 message-main-receiver">'+
'<div class="receiver">'+
'<div class="message-text"></div>'+
'<span class="message-time pull-right"></div>'+
'</div></div></div>';


var OUTGOING_TEMPLATE =
'<div class="row message-body">' +
'<div class="col-sm-12 message-main-sender">' +
'<div class="sender">' +
'<div class="message-text"></div>'+
'<span class="message-time pull-right"></div>'+
'</div></div></div>';

// Adds a size to Google Profile pics URLs.
function addSizeToGoogleProfilePic(url) {
    if (url.indexOf('googleusercontent.com') !== -1 && url.indexOf('?') === -1) {
        return url + '?sz=150';
    }
    return url;
}

// A loading image URL.
var LOADING_IMAGE_URL = 'https://www.google.com/images/spin-32.gif?a';

// Delete a Message from the UI.
function deleteMessage(id) {
    var div = document.getElementById(id);
    // If an element for that message exists we delete it.
    if (div) {
        div.parentNode.removeChild(div);
    }
}

function createAndInsertMessage(id, date, time, timestamp, messageType) {
    const container = document.createElement('div');
    if(messageType == "incoming") {
        container.innerHTML = INCOMING_TEMPLATE;
    } else {
        container.innerHTML = OUTGOING_TEMPLATE;
    }
    const div = container.firstChild;
    div.setAttribute('id', id);
    var displayTime = date+" "+time;
    div.setAttribute('timestamp', timestamp);
    const existingMessages = messageListElement.children;
    if (existingMessages.length === 0) {

        messageListElement.appendChild(div);

    } else {

        let messageListNode = existingMessages[0];

        while (messageListNode) {
            const messageListNodeTime = messageListNode.getAttribute('timestamp');
            if (!messageListNodeTime) {
                throw new Error(
                    `Child ${messageListNode.id} has no 'timestamp' attribute`
            );
          }

          if (messageListNodeTime > timestamp) {
            break;
          }

          messageListNode = messageListNode.nextSibling;
        }
        messageListElement.insertBefore(div, messageListNode);

    }
    return div;
}

// Displays a Message in the UI.
function displayMessage(id, date, time, text, timestamp, type, messageType, imageUrl) {
    
    var div = document.getElementById(id) || createAndInsertMessage(id, date, time, timestamp, messageType);
    var messageElement = div.querySelector('.message-text');
    var timeStampElement = div.querySelector('.message-time');
    timeStampElement.textContent = time+ " | "+date;

    if (type == "text") { // If the message is text.
        messageElement.textContent = text;
        // Replace all line breaks by <br>.
        messageElement.innerHTML = messageElement.innerHTML.replace(/\n/g, '<br>');
    } else if (type == "image") { // If the message is an image.
        messageElement.innerHTML = '';
        var anchorTag = document.createElement('a');
        anchorTag.href = text; 
        anchorTag.setAttribute('target', '_blank');
        var image = document.createElement('img');
        image.addEventListener('load', function() {
            messageListElement.scrollTop = messageListElement.scrollHeight;
        });
        image.src = text + '&' + new Date().getTime();
        image.className = "img-responsive";
        anchorTag.appendChild(image);
        messageElement.appendChild(anchorTag);
    } else if (type == "pdf") { // If the message is an image.

        var urlPath = $('#siteUrl').val();
        messageElement.innerHTML = '';
        var anchorTag = document.createElement('a');
        anchorTag.href = text; 
        anchorTag.setAttribute('target', '_blank');
        var image = document.createElement('img');
        image.addEventListener('load', function() {
            messageListElement.scrollTop = messageListElement.scrollHeight;
        });
        var pdfSrc = urlPath+"assets/vc/images/pdf.png";
        image.src = pdfSrc;
        image.className = "img-responsive";
        anchorTag.appendChild(image);
        messageElement.appendChild(anchorTag);
    } 
    // Show the card fading-in and scroll to view the new message.
    setTimeout(function() {div.classList.add('visible')}, 1);
    messageListElement.scrollTop = messageListElement.scrollHeight;
    messageInputElement.focus();
    
}

// Enables or disables the submit button depending on the values of the input
// fields.
function toggleButton() {
    if (messageInputElement.value) {
        submitButtonElement.removeAttribute('disabled');
    } else {
        submitButtonElement.setAttribute('disabled', 'true');
    }
}

// Checks that the Firebase SDK has been correctly setup and configured.
function checkSetup() {
    if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
        window.alert('You have not configured and imported the Firebase SDK. ' +
        'Make sure you go through the codelab setup instructions and make ' +
        'sure you are running the codelab using `firebase serve`');
    }
}

// Checks that Firebase has been imported.
checkSetup();

// Shortcuts to DOM Elements.
var messageListElement = document.getElementById('messages');
var messageFormElement = document.getElementById('message-form');
var messageInputElement = document.getElementById('message');
var submitButtonElement = document.getElementById('submit');
var imageButtonElement = document.getElementById('submitImage');
var fileButtonElement = document.getElementById('submitFile');
var imageFormElement = document.getElementById('image-form');
var mediaCaptureElement = document.getElementById('mediaCapture');
var fileCaptureElement = document.getElementById('fileCapture');
// var userPicElement = document.getElementById('user-pic');
// var userNameElement = document.getElementById('user-name');
// var signInButtonElement = document.getElementById('sign-in');
// var signOutButtonElement = document.getElementById('sign-out');
// var signInSnackbarElement = document.getElementById('must-signin-snackbar');

// Saves message on form submit.
messageFormElement.addEventListener('submit', onMessageFormSubmit);
// signOutButtonElement.addEventListener('click', signOut);
// signInButtonElement.addEventListener('click', signIn);

// Toggle for the button.
messageInputElement.addEventListener('keyup', toggleButton);
messageInputElement.addEventListener('change', toggleButton);

// Events for image upload.
imageButtonElement.addEventListener('click', function(e) {
    e.preventDefault();
    mediaCaptureElement.click();
});
mediaCaptureElement.addEventListener('change', onMediaFileSelected);


// Events for image upload.
fileButtonElement.addEventListener('click', function(e) {
    e.preventDefault();
    fileCaptureElement.click();
});
fileCaptureElement.addEventListener('change', onPdfFileSelected);

// initialize Firebase
initFirebaseAuth();

// TODO: Enable Firebase Performance Monitoring.

// We load currently existing chat messages and listen to new ones.
signIn();
